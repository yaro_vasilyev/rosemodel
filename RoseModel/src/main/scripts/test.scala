
/*
 * Created with IntelliJ IDEA.
 * User: yaro
 * Date: 14.06.13
 * Time: 22:51
 */



import java.io.{File, InputStreamReader, FileInputStream, FilenameFilter, FileWriter}
import com.toy.rosemodel.RoseParser

//val r = parser.parseModel(new InputStreamReader(
//                      new FileInputStream("""C:\src\rosemodel\ref\model\business.mdl"""),
//                      "cp1251"))


def parseCatalog(fileName: String) {
  val parser = new RoseParser

  println (s"Parsing '$fileName'")

  val r = parser.parseCatalog(new InputStreamReader(
                        new FileInputStream(fileName),
                        "cp1251"))

  if (r.successful)
    println (r.get)
  else
    println (r)
}


def parseAll(log: String, deleteLogFirst: Boolean): Unit = {
  def traverseFileSystem(directory: File,
                         filter: FilenameFilter,
                         action: (File) => Unit) {
    for(fn <- directory.list(filter)) {
      val f = new File(directory, fn)
      if (f.isDirectory)
        traverseFileSystem(f, filter, action)
      else {
        if (filter.accept(directory, fn))
          action(f)
      }
    }
  }


  val logFile = new File(log)
  if (deleteLogFirst)
    logFile.delete()

  val out = new FileWriter(log)
  try {
    traverseFileSystem(new File("""C:\src\rose"""),
      new FilenameFilter {
        def accept(dir: File, name: String): Boolean = {
          dir.getName != ".svn"
        }
      },
      f => {
        if (f.getName.endsWith(".cat")) {
          try {
            val parser = new RoseParser
            val r = parser.parseCatalog(new InputStreamReader(
                                new FileInputStream(f),
                                "cp1251"))
            if (r.successful) {
              out.append(s"OK: ${f}\n")
              println (s"OK: ${f}")
            } else {
              out.append(s"FAIL: ${f}\n${r}\n")
              println (s"FAIL: ${f}\n${r}")
            }
          } catch {
            case e: Exception =>
              out.append("FAIL: %s\n%s\n".format(f, e.getMessage))
              println ("Error in file parsing, file = %s", f)
              println (e)
          }
        }
      }
    )
  } finally {
    out.close()
  }

}

def parseModel(fileName: String): Unit = {
  val p = new RoseParser
  val m = p.parseModel(new InputStreamReader(new FileInputStream("""C:\src\business.mdl"""), "cp1251"))

  if (m.successful)
    println (m.get)
  else
    println (m)
}

parseAll("""C:\tmp\parsing.log""", true)
//parseCatalog("""C:\src\rose\AccountingUseCaseModel.cat""")
//parseModel("""C:\src\business.mdl""")