package com.toy.rosemodel

/*
 * Created with IntelliJ IDEA.
 * User: yaro
 * Date: 16.06.13
 * Time: 19:58
 */

trait UnitReference


class BaseAttribute(tool: String, name: String, val value: Any)


case class Attribute[T](tool: String, name: String, override val value: T)
  extends BaseAttribute(tool, name, value) {

  override def toString: String = s"Attribute(tool: $tool, name: $name, value: $value)"
}


case class DesignDefaults(rightMargin: Double,
                          leftMargin: Double,
                          topMargin: Double,
                          bottomMargin: Double,
                          pageOverlap: Double,
                          clipIconLabels: Boolean,
                          autoResize: Boolean,
                          snapToGrid: Boolean,
                          gridX: Int,
                          gridY: Int,
                          defaultFont: Font,
                          showMessageNum: Int,
                          showClassOfObject: Boolean,
                          notation: String)



case class ClassCategory(name: String,
                         quid: String,
                         stereotype: String,
                         exportControl: String,
                         globla: Boolean,
                         subsystem: String,
                         quidu: String,
                         isUnit: Boolean,
                         isLoaded: Boolean,
                         fileName: String,
                         visibleModules: Seq[DependencyRelationship],
                         logicalModules: Seq[UnitReference],
                         logicalPresentations: Seq[UnitReference],
                         documentation: String,
                         stateMachine: StateMachine,
                         externalDocs: Seq[ExternalDoc]) extends UnitReference

case class ClassCategoryRef(name: String,
                            isUnit: Boolean,
                            isLoaded: Boolean,
                            fileName: String,
                            quid: String) extends UnitReference



case class Association(name: String,
                       quid: String,
                       stereotype: String,
                       documentation: String,
                       roles: Seq[Role]) extends UnitReference

case class Class(name: String,
                 quid: String,
                 externalDocs: Seq[ExternalDoc],
                 exportControl: String,
                 stateMachine: StateMachine,
                 documentation: String,
                 stereotype: String,
                 superclasses: Seq[InheritanceRelationship],
                 realizedInterfaces: Seq[RealizeRelationship],
                 classAttributes: Seq[ClassAttribute],
                 operations: Seq[Operation],
                 visibleModules: Seq[DependencyRelationship],
                 module: String,
                 quidu: String,
                 usedNodes: Seq[UsesRelationship],
                 nestedClasses: Seq[Class],
                 persistence: String,
                 cardinality: String,
                 isAbstract: Boolean) extends UnitReference

case class ActivityState(name: String,
                         quid: String,
                         stereotype: String,
                         documentation: String,
                         externalDocs: Seq[ExternalDoc],
                         actions: Seq[Action],
                         stateMachine: StateMachine) extends StateListItem

case class ActionTime(when: String)

case class Action(name: String,
                  quid: String,
                  actionTime: ActionTime)



case class InterMessView(name: String, refTo: Int,
                         location: Point,
                         font: Font,
                         label: SegLabel,
                         lineColor: Int,
                         client: Int,
                         supplier: Int,
                         focusSrc: Int,
                         focusEntry: Int,
                         origin: Point,
                         terminus: Point,
                         ordinal: Int) extends View


case class UsesView(name: String, refTo: Int,
                    font: Font,
                    label: SegLabel,
                    stereotype: SegLabel,
                    lineColor: Int,
                    quidu: String,
                    client: Int,
                    supplier: Int,
                    vertices: Seq[Point],
                    lineStyle: Int,
                    originAttachment: Point,
                    terminalAttachment: Point) extends View


case class ExternalDoc(externalDocPath: String)


case class _Object(name: String,
                   quid: String,
                   collaborators: Seq[Link],
                   `class`: String,
                   quidu: String,
                   persistence: String,
                   creationObj: Boolean,
                   multi: Boolean) extends UnitReference

case class Mechanism(refTo: Int,
                     logicalModels: Seq[UnitReference]) extends UnitReference


case class Subsystem(name: String,
                     quid: String,
                     physicalModels: Seq[UnitReference],
                     physicalPresentations: Seq[UnitReference],
                     category: String,
                     quidu: String)


case class Design(name: String,
                  isUnit: Boolean,
                  isLoaded: Boolean,
                  attributes: Seq[BaseAttribute],
                  quid: String,
                  enforceClosureAutoLoad: Boolean,
                  defaults: DesignDefaults,
                  rootUsecasePackage: ClassCategory,
                  rootCategory: ClassCategory,
                  rootSubsystem: Subsystem,
                  process_structure: Processes,
                  properties: Properties)

case class Compartment(parentView: Int,
                       location: Point,
                       font: Font,
                       iconStyle: String,
                       fillColor: Int,
                       anchor: Int,
                       nlines: Int,
                       maxWidth: Int,
                       justify: Int)


case class Font(size: Int,
                face: String,
                charSet: Int,
                bold: Boolean,
                italics: Boolean,
                underline: Boolean,
                strike: Boolean,
                color: Int,
                defaultColor: Boolean)


case class Model(petal: Petal, design: Design)


case class Operation(name: String,
                     quid: String,
                     documentation: String,
                     externalDocs: Seq[ExternalDoc],
                     parameters: Seq[Parameter],
                     result: String,
                     stereotype: String,
                     concurrency: String,
                     `abstract`: Boolean,
                     opExportControl: String,
                     uid: Int,
                     quidu: String,
                     stateMachine: StateMachine)


case class Parameter(name: String, quid: String,
                     documentation: String,
                     `type`: String, quidu: String)

case class DependencyView(name: String, refTo: Int,
                          font: Font,
                          label: ItemLabel,
                          stereotype: Boolean,
                          lineColor: Int,
                          quidu: String,
                          client: Int,
                          supplier: Int,
                          vertices: Seq[Point],
                          lineStyle: Int) extends View

case class AssocConstraintView(name: String, refTo: Int,
                               font: Font,
                               stereotype: Boolean,
                               lineColor: Int,
                               client: Int,
                               supplier: Int,
                               vertices: Seq[Point],
                               lineNumber: Int,
                               originAttachment: Point,
                               terminalAttachment: Point) extends View



case class Petal(version: Int,
                 _written: String,
                 charSet: Int)

case class Properties(attributes: Seq[BaseAttribute], quid: String)


case class Point(x: Int, y: Int)


case class Module(name: String,
                  modType: String,
                  modPart: String,
                  quid: String,
                  stereotype: String,
                  language: String,
                  `class`: String,
                  quidu: String,
                  class2: String,
                  quidu2: String) extends UnitReference


case class Role(name: String,
                quid: String,
                label: String,
                supplier: String,
                quidu: String,
                exportControl: String,
                containment: String,
                isAggregate: Boolean,
                clientCardinality: String,
                isNavigable: Boolean)

case class ClassUtility(name: String,
                        quid: String,
                        stereotype: String,
                        externalDocs: Seq[ExternalDoc]) extends UnitReference


case class UsesRelationship(quid: String,
                            stereotype: String,
                            label: String,
                            supplier: String,
                            quidu: String)


case class DependencyRelationship(quid: String,
                                  label: String,
                                  supplier: String,
                                  quidu: String)

case class InheritanceRelationship(quid: String,
                                   label: String,
                                   supplier: String,
                                   quidu: String)

case class RealizeRelationship(quid: String,
                               label: String,
                               supplier: String,
                               quidu: String)


abstract class View


class ClassView extends View {
  var `type`: String = null
  var name: String = null
  var refTo: Int = -1
  var showCompartmentStereotypes: Boolean = false
  var showOperationSignature: Boolean = false
  var suppressAttribute: Boolean = false
  var suppressOperation: Boolean = false
  var includeAttribute: Boolean = false
  var includeOperation: Boolean = false
  var location: Point = null
  var font: Font = null
  var label: ItemLabel = null
  var stereotype: ItemLabel = null
  var icon: String = null
  var iconStyle: String = null
  var lineColor: Int = -1
  var fillColor: Int = -1
  var quidu: String = null
  var compartment: Compartment = null
  var width: Int = -1
  var height: Int = -1
  var annotation: Int = -1
  var autoResize: Boolean = false
}

case class ModView(name: String,
                   modType: String,
                   modPart: String,
                   refTo: Int,
                   location: Point,
                   font: Font,
                   label: ItemLabel,
                   iconStyle: String,
                   lineColor: Int,
                   fillColor: Int,
                   quidu: String,
                   width: Int,
                   height: Int) extends View


  case class RealizeView(name: String, refTo: Int,
                          font: Font,
                          label: SegLabel,
                          stereotype: Boolean,
                          lineColor: Int,
                          quidu: String,
                          client: Int,
                          supplier: Int,
                          vertices: Seq[Point],
                          lineStyle: Int) extends View



case class CategoryView(name: String,
                        refTo: Int,
                        location: Point,
                        font: Font,
                        label: ItemLabel,
                        stereotype: ItemLabel,
                        icon: String,
                        iconStyle: String,
                        lineColor: Int,
                        fillColor: Int,
                        quidu: String,
                        width: Int,
                        height: Int) extends View



abstract class Diagram(name: String,
                       quid: String,
                       title: String,
                       zoom: Int,
                       maxHeight: Int, maxWidth: Int,
                       originX: Int, originY: Int,
                       items: Seq[View]) extends UnitReference

case class UseCaseDiagram(name: String,
                          quid: String,
                          title: String,
                          doc: String,
                          zoom: Int,
                          maxHeight: Int, maxWidth: Int,
                          originX: Int, originY: Int,
                          items: Seq[View]) extends Diagram(name,
                                                            quid,
                                                            title,
                                                            zoom,
                                                            maxHeight, maxWidth,
                                                            originX, originY,
                                                            items)

case class ProcessDiagram(name: String,
                          quid: String,
                          title: String,
                          zoom: Int,
                          maxHeight: Int, maxWidth: Int,
                          originX: Int, originY: Int,
                          items: Seq[View]) extends Diagram(name,
                                                            quid,
                                                            title,
                                                            zoom,
                                                            maxHeight, maxWidth,
                                                            originX, originY,
                                                            items)


case class Processes(quid: String,
                     procNdevs: Seq[ProcessDiagram])



case class ClassDiagram(name: String,
                        quid: String,
                        title: String,
                        documentation: String,
                        externalDocs: Seq[ExternalDoc],
                        zoom: Int,
                        maxHeight: Int, maxWidth: Int,
                        originX: Int, originY: Int,
                        items: Seq[View]) extends Diagram(name,
                                                          quid,
                                                          title,
                                                          zoom,
                                                          maxHeight, maxWidth,
                                                          originX, originY,
                                                          items)

case class ModuleDiagram(name: String,
                         quid: String,
                         title: String,
                         zoom: Int,
                         maxHeight: Int, maxWidth: Int,
                         originX: Int, originY: Int,
                         items: Seq[View]) extends Diagram(name,
                                                            quid,
                                                            title,
                                                            zoom,
                                                            maxHeight, maxWidth,
                                                            originX, originY,
                                                            items)

case class ClassAttribute(name: String,
                          quid: String,
                          documentation: String,
                          stereotype: String,
                          `type`: String,
                          quidu: String,
                          initv: String,
                          exportControl: String,
                          derived: Boolean)



case class Swimlane(name: String,
                    refTo: Int,
                    location: Point,
                    lineColor: Int,
                    quidu: String,
                    width: Int) extends View

class Label(refTo: Int,
            parentView: Int,
            location: Point,
            font: Font,
            anchor: Int,
             nlines: Int,
             maxWidth: Int,
             justify: Int,
             label: String) extends View


case class ItemLabel(parentView: Int,
                     location: Point,
                     fillColor: Int,
                     anchor: Int,
                     anchorLoc: Int,
                     nlines: Int,
                     maxWidth: Int,
                     justify: Int,
                     label: String) extends Label(-1, parentView, location, null, anchor, nlines, maxWidth, justify, label)


case class SegLabel(refTo: Int,
                    parentView: Int,
                    location: Point,
                    font: Font,
                    quidu: String,
                    anchor: Int,
                    anchorLoc: Int,
                    hidden: Boolean,
                    nlines: Int,
                    maxWidth: Int,
                    justify: Int,
                    label: String,
                    pctDist: Double,
                    height: Int,
                    orientation: Int) extends Label(refTo, parentView, location, font, anchor, nlines, maxWidth, justify, label)

case class State(name: String,
                 quid: String,
                 documentation: String,
                 `type`: String) extends StateListItem

case class ObjectFlow()

case class SendEvent(quid: String)

case class Event(name: String,
                 quid: String,
                 parameters: String)

case class RoseObject()

case class Decision(name: String,
                    quid: String,
                    documentation: String) extends StateListItem


trait StateListItem

case class StateTransition(quid: String,
                           documentation: String,
                           label: String,
                           supplier: String,
                           supplierQuidu: String,
                           client: String,
                           clientQuidu: String,
                           event: Event,
                           condition: String,
                           sendEvent: SendEvent)


case class StateView(name: String,
                     desc: String,
                     refTo: Int,
                     parentView: Int,
                     location: Point,
                     font: Font,
                     label: Label,
                     iconStyle: String,
                     lineColor: Int,
                     fillColor: Int,
                     quidu: String,
                     width: Int,
                     height: Int,
                     autoResize: Boolean) extends View

case class NoteView(refTo: Int,
                    parentView: Int,
                    location: Point,
                    font: Font,
                    label: ItemLabel,
                    lineColor: Int,
                    fillColor: Int,
                    width: Int,
                    height: Int,
                    quidu: String) extends View

case class AttachView(name: String, refTo: Int,
                      stereotype: Boolean,
                      lineColor: Int,
                      client: Int,
                      supplier: Int,
                      vertices: Seq[Point],
                      lineStyle: Int) extends View


case class DecisionView(name: String, refTo: Int,
                        parentView: Int,
                        location: Point,
                        font: Font,
                        label: SegLabel,
                        iconStyle: String,
                        lineColor: Int,
                        fillColor: Int,
                        quidu: String,
                        width: Int,
                        height: Int,
                        autoResize: Boolean) extends View

case class TransView(name: String, refTo: Int,
                     font: Font,
                     label: SegLabel,
                     stereotype: Boolean,
                     lineColor: Int,
                     quidu: String,
                     client: Int,
                     supplier: Int,
                     vertices: Seq[Point],
                     lineStyle: Int,
                     originAttachment: Point,
                     terminalAttachment: Point,
                     xOffset: Boolean) extends View


case class ActivityDiagram(name: String,
                           quid: String,
                           title: String,
                           documentation: String,
                           zoom: Int,
                           maxHeight: Int,
                           maxWidth: Int,
                           originX: Int,
                           originY: Int,
                           items: Seq[View]) extends StateDiagramListItem

case class StateDiagram(name: String,
                        quid: String,
                        title: String,
                        zoom: Int,
                        maxHeight: Int,
                        maxWidth: Int,
                        originX: Int,
                        originY: Int,
                        items: Seq[View]) extends StateDiagramListItem


trait StateDiagramListItem

case class StateMachine(name: String,
                        quid: String,
                        states: Seq[StateListItem],
                        partitions: Seq[Partition],
                        objects: Seq[RoseObject],
                        transitions: Seq[StateTransition],
                        objectFlows: Seq[ObjectFlow],
                        stateDiagrams: Seq[StateDiagramListItem])


case class RoleView(name: String, refTo: Int,
                    parentView: Int,
                    location: Point,
                    font: Font,
                    label: SegLabel,
                    stereotype: Boolean,
                    lineColor: Int,
                    quidu: String,
                    client: Int,
                    supplier: Int,
                    vertices: Seq[Point],
                    lineStyle: Int,
                    originAttachment: Point,
                    terminalAttachment: Point,
                    label2: SegLabel)


case class AssociationViewNew(name: String, refTo: Int,
                              location: Point,
                              font: Font,
                              label: SegLabel,
                              stereotype: Any, // Boolean|SegLabel
                              lineColor: Int,
                              quidu: String,
                              roleviewList: Seq[RoleView]) extends View


case class InheritView(name: String, refTo: Int,
                       font: Font,
                       label: SegLabel,
                       stereotype: Boolean,
                       lineColor: Int,
                       quidu: String,
                       client: Int,
                       supplier: Int,
                       vertices: Seq[Point],
                       lineStyle: Int,
                       originAttachment: Point,
                       terminalAttachment: Point,
                       drawSupplier: Int) extends View


case class Partition(name: String,
                     quid: String,
                     persistence: String,
                     creationObj: Boolean,
                     multi: Boolean)


case class ActivityStateView(name: String, refTo: Int,
                             parentView: Int,
                             location: Point,
                             font: Font,
                             label: ItemLabel,
                             stereotype: ItemLabel,
                             iconStyle: String,
                             lineColor: Int,
                             fillColor: Int,
                             quidu: String,
                             width: Int,
                             height: Int,
                             annotation: Int,
                             autoResize: Boolean) extends View


case class InheritTreeView(name: String, refTo: Int,
                           location: Point,
                           lineColor: Int,
                           fillColor: Int,
                           supplier: Int,
                           vertices: Seq[Point]) extends View




case class UseCase(name: String,
                   quid: String,
                   documentation: String,
                   attributes: Seq[BaseAttribute],
                   stereotype: String,
                   superclasses: Seq[InheritanceRelationship],
                   realizedInterfaces: Seq[RealizeRelationship],
                   visibleModules: Seq[DependencyRelationship],
                   logicalModels: Seq[UnitReference],
                   logicalPresentations: Seq[UnitReference]) extends UnitReference

case class Message(name: String,
                   quid: String,
                   documentation: String,
                   frequency: String,
                   synchronization: String,
                   dir: String,
                   sequence: String,
                   ordinal: Int,
                   quidu: String,
                   creation: Boolean)

case class Link(quid: String,
                supplier: String,
                quidu: String,
                messages: Seq[Message])


case class SelfMessView(name: String,
                        refTo: Int,
                        location: Point,
                        font: Font,
                        label: SegLabel,
                        lineColor: Int,
                        client: Int,
                        supplier: Int,
                        focusSrc: Int,
                        focusEntry: Int,
                        origin: Point,
                        terminus: Point,
                        ordinal: Int) extends View



case class FocusOfControl(name: String, refTo: Int,
                          location: Point,
                          lineColor: Int,
                          interObjView: Int,
                          height: Int,
                          yCoord: Int,
                          nested: Boolean)


case class InterObjView(name: String, refTo: Int,
                        location: Point,
                        font: Font,
                        label: ItemLabel,
                        icon: String,
                        iconStyle: String,
                        lineColor: Int,
                        fillColor: Int,
                        quidu: String,
                        width: Int,
                        height: Int,
                        iconHeight: Int,
                        iconWidth: Int,
                        iconYOffset: Int,
                        annotation: Int,
                        focusOfControls: Seq[FocusOfControl]) extends View


case class InteractionDiagram(name: String,
                              mechanismRef: Int,
                              quid: String,
                              title: String,
                              documentation: String,
                              zoom: Int,
                              maxHeight: Int,
                              maxWidth: Int,
                              originX: Int,
                              originY: Int,
                              items: Seq[View]) extends UnitReference


case class UseCaseView(name: String, refTo: Int,
                       location: Point,
                       font: Font,
                       label: ItemLabel,
                       icon: String,
                       iconStyle: String,
                       lineColor: Int,
                       fillColor: Int,
                       quidu: String,
                       width: Int,
                       height: Int) extends View
