package com.toy.rosemodel

import scala.util.parsing.combinator.JavaTokenParsers
import java.io.{Reader, FileReader}

/*
 * Created with IntelliJ IDEA.
 * User: Null
 * Date: 18.06.13
 * Time: 9:43
 */


class RoseParser extends JavaTokenParsers {

  /*
  model = object_petal object_design
  */
  def modelParser: Parser[Model] = petalParser ~
    designParser ^^ {
    case p ~ d => Model(p, d)
  }


  /*
    object_petal = "(object" "Petal"
                      "version" number
                      "_written" string
                      "charSet" number
                   ")"
  */
  def petalParser: Parser[Petal] = "(object" ~> "Petal" ~>
                                      "version" ~ wholeNumber ~
                                      "_written" ~ stringLiteral ~
                                      "charSet" ~ wholeNumber <~
                                      ")" ^^ {
    case "version" ~ v ~
          "_written" ~ w ~
          "charSet" ~ cs => Petal(v.toInt, w, cs.toInt)
  }




  /*
    object_design = "(object" "Design" string
                      "is_unit" bool
                      "is_loaded" bool
                      "attributes" list_attr_set
                      "quid" quid_str
                      "enforceClosureAutoLoad" bool
                      "defaults" object_design_defaults
                      "root_usecase_package" object_class_category
                      "root_category" object_class_category
                      "root_subsystem" object_sub_system
                      "process_structure" object_processes
                      "properties" object_properties
                    ")"
  */
  def designParser: Parser[Design] = "(object" ~> "Design" ~> stringLiteral ~
                                      "is_unit" ~ bool ~
                                      "is_loaded" ~ bool ~
                                      "attributes" ~ attrListParser ~
                                      "quid" ~ quid ~
                                      "enforceClosureAutoLoad" ~ bool ~
                                      "defaults" ~ designDefaultsParser ~
                                      "root_usecase_package" ~ classCategoryParser ~
                                      "root_category" ~ classCategoryParser ~
                                      "root_subsystem" ~ subsystemParser ~
                                      "process_structure" ~ processesParser ~
                                      "properties" ~ propertiesParser <~
                                      ")" ^^ {
    case name ~
          "is_unit" ~ un ~
          "is_loaded" ~ l ~
          "attributes" ~ at ~
          "quid" ~ q ~
          "enforceClosureAutoLoad" ~ enf ~
          "defaults" ~ dflt ~
          "root_usecase_package" ~ useCase ~
          "root_category" ~ cat ~
          "root_subsystem" ~ ss ~
          "process_structure" ~ str ~
          "properties" ~ prop => Design(name,
                                        un,
                                        l,
                                        at,
                                        q,
                                        enf,
                                        dflt,
                                        useCase,
                                        cat,
                                        ss,
                                        str,
                                        prop)
  }



  /*
    object_properties = "(object" "Properties" \
                            "attributes" list_attr_set \
                            "quid" quid_str
                         ")"
  */
  def propertiesParser: Parser[Properties] = "(object" ~> "Properties" ~>
                                              "attributes" ~ attrListParser ~
                                              "quid" ~ quid <~
                                              ")" ^^ {
    case "attributes" ~ at ~ "quid" ~ q => Properties(at, q)
  }

  /*
    object_processes = "(object" "Processes"
                          "quid" quid_str
                          "ProcsNDevs" list
                        ")"
  */
  def processesParser: Parser[Processes] = "(object" ~> "Processes" ~>
                                            "quid" ~ quid ~
                                            "ProcsNDevs" ~ processDiagramListParser <~
                                            ")" ^^ {
    case "quid" ~ q ~
          "ProcsNDevs" ~ list => Processes(q, list)
  }

  /*
    list = "(list" { object_proc_diagram } ")"
  */
  def processDiagramListParser: Parser[Seq[ProcessDiagram]] =
    "(list" ~>
      rep(processDiagramParser) <~
      ")"

  /*
    object_proc_diagram = "(object" "Process_Diagram" string
                              "quid" quid_str
                              "title" string
                              "zoom" number
                              "max_height" number
                              "max_width" number
                              "origin_x" number
                              "origin_y" number
                              "items" list_diagram_item
                          ")"
  */
  def processDiagramParser: Parser[ProcessDiagram] = "(object" ~> "Process_Diagram" ~> stringLiteral ~
                                                      "quid" ~ quid ~
                                                      "title" ~ stringLiteral ~
                                                      "zoom" ~ wholeNumber ~
                                                      "max_height" ~ wholeNumber ~
                                                      "max_width" ~ wholeNumber ~
                                                      "origin_x" ~ wholeNumber ~
                                                      "origin_y" ~ wholeNumber ~
                                                      "items" ~ diagramItemListParser <~
                                                      ")" ^^ {
    case name ~
          "quid" ~ q ~
          "title" ~ t ~
          "zoom" ~ z ~
          "max_height" ~ h ~
          "max_width" ~ w ~
          "origin_x" ~ x ~
          "origin_y" ~ y ~
          "items" ~ it => ProcessDiagram(name,
                                          q,
                                          t,
                                          z.toInt,
                                          h.toInt, w.toInt,
                                          x.toInt, y.toInt,
                                          it)
  }

  /*
  object_sub_system = "(object" "SubSystem" string \
                         "quid" quid_str \
                         "physical_models" list_unit_ref \
                         "physical_presentations" list_unit_ref \
                         "category" string \
                         "quidu" quid_str
                       ")"
  */
  def subsystemParser: Parser[Subsystem] = "(object" ~> "SubSystem" ~> stringLiteral ~
                                            "quid" ~ quid ~
                                            "physical_models" ~ unitReferenceListParser ~
                                            "physical_presentations" ~ unitReferenceListParser ~
                                            "category" ~ stringLiteral ~
                                            "quidu" ~ quid <~
                                            ")" ^^ {
    case name ~
        "quid" ~ q ~
        "physical_models" ~ models ~
        "physical_presentations" ~ presentations ~
        "category" ~ cat ~
        "quidu" ~ qu => Subsystem(name,
                                  q,
                                  models,
                                  presentations,
                                  cat,
                                  qu)
  }


  /*
    object_design_defaults = "(object" "defaults"
                                "rightMargin" decimal
                                "leftMargin" decimal
                                "topMargin" decimal
                                "bottomMargin" decimal
                                "pageOverlap" decimal
                                "clipIconLabels" bool
                                "autoResize" bool
                                "snapToGrid" bool
                                "gridX" number
                                "gridY" number
                                "defaultFont" object_font
                                "showMessageNum" number
                                "showClassOfObject" bool
                                "notation" string
                             ")"
  */
  def designDefaultsParser: Parser[DesignDefaults] = "(object" ~> "defaults" ~>
                                                      "rightMargin" ~ decimalNumber ~
                                                      "leftMargin" ~ decimalNumber ~
                                                      "topMargin" ~ decimalNumber ~
                                                      "bottomMargin" ~ decimalNumber ~
                                                      "pageOverlap" ~ decimalNumber ~
                                                      "clipIconLabels" ~ bool ~
                                                      "autoResize" ~ bool ~
                                                      "snapToGrid" ~ bool ~
                                                      "gridX" ~ wholeNumber ~
                                                      "gridY" ~ wholeNumber ~
                                                      "defaultFont" ~ fontParser ~
                                                      "showMessageNum" ~ wholeNumber ~
                                                      "showClassOfObject" ~ bool ~
                                                      "notation" ~ stringLiteral <~ ")" ^^ {
    case "rightMargin" ~ rm ~
          "leftMargin" ~ lm ~
          "topMargin" ~ tm ~
          "bottomMargin" ~ bm ~
          "pageOverlap" ~ po ~
          "clipIconLabels" ~ clipLab ~
          "autoResize" ~ ar ~
          "snapToGrid" ~ snap ~
          "gridX" ~ x ~
          "gridY" ~ y ~
          "defaultFont" ~ font ~
          "showMessageNum" ~ msg ~
          "showClassOfObject" ~ cls ~
          "notation" ~ note => DesignDefaults(rm.toDouble, lm.toDouble, tm.toDouble, bm.toDouble,
                                              po.toDouble,
                                              clipLab,
                                              ar,
                                              snap,
                                              x.toInt, y.toInt,
                                              font,
                                              msg.toInt,
                                              cls,
                                              note)
  }


  /*
  object_class_category = "(object" "Class_Category" string
                              [ "is_unit"     bool ]
                              [ "is_loaded"   bool ]
                              [ "quid"        quid_str ]
                              [ "documentation text_value ]
                              [ "stereotype"   string ]
                              [ "external_docs" list_external_doc ]
                              [ "exportControl"  string
                              [ "global" bool ]
                              [ "subsystem" string ]
                              [ "quidu" quid_str ]
                              [ "is_unit" bool ]
                              [ "is_loaded" bool ]
                              [ "file_name" string ]
                              [ "visible_modules" list_dependency ]
                              [ "logical_models" list_unit_ref ]
                              [ "logical_presentations" list_unit_ref ]
                          ")"
  */
  def classCategoryParser: Parser[ClassCategory] = "(object" ~> "Class_Category" ~> stringLiteral ~
                                                      opt("is_unit" ~> bool) ~
                                                      opt("is_loaded" ~> bool) ~
                                                      opt("quid" ~> quid) ~
                                                      opt("documentation" ~> textValue) ~
                                                      opt("stereotype" ~> stringLiteral) ~
                                                      opt("external_docs" ~> externalDocListParser) ~
                                                      opt("visible_modules" ~> dependencyListParser) ~
                                                      opt("exportControl" ~> stringLiteral) ~
                                                      opt("global" ~> bool) ~
                                                      opt("subsystem" ~> stringLiteral) ~
                                                      opt("quidu" ~> quid) ~
                                                      opt("file_name" ~> stringLiteral) ~
                                                      opt("logical_models" ~> unitReferenceListParser) ~
                                                      opt("statemachine" ~> stateMachineParser) ~
                                                      opt("logical_presentations" ~> unitReferenceListParser) <~
                                                    ")" ^^ {
    case name ~
          un ~
          l ~
          q ~
          doc ~
          st ~
          extDoc ~
          visMod ~
          exp ~
          g ~
          ss ~
          qu ~
          fn ~
          mod ~
          sm ~
          pres => ClassCategory(name,
                                if (q.isDefined) q.get else null,
                                if (st.isDefined) st.get else null,
                                if (exp.isDefined) exp.get else null,
                                if (g.isDefined) g.get else false,
                                if (ss.isDefined) ss.get else null,
                                if (qu.isDefined) qu.get else null,
                                if (un.isDefined) un.get else false,
                                if (l.isDefined) l.get else false,
                                if (fn.isDefined) fn.get else null,
                                if (visMod.isDefined) visMod.get else null,
                                if (mod.isDefined) mod.get else null,
                                if (pres.isDefined) pres.get else null,
                                if (doc.isDefined) doc.get else null,
                                if (sm.isDefined) sm.get else null,
                                if (extDoc.isDefined) extDoc.get else Array[ExternalDoc]())
  }

  /*
  object_class_category_ref = "(object" "Class_Category" string
                                "is_unit" bool
                                "is_loaded" bool
                                "file_name" string
                                "quid" quad_str
                               ")"
  */
  def classCategoryRefParser: Parser[ClassCategoryRef] = "(object" ~> "Class_Category" ~> stringLiteral ~
                                                          "is_unit" ~ bool ~
                                                          "is_loaded" ~ bool ~
                                                          "file_name" ~ stringLiteral ~
                                                          "quid" ~ quid <~
                                                          ")" ^^ {
    case name ~
        "is_unit" ~ un ~
        "is_loaded" ~ ld ~
        "file_name" ~ fn ~
        "quid" ~ q => ClassCategoryRef(name,
                                        un,
                                        ld,
                                        fn,
                                        q)
  }

  /*
  object_message = "(object" "Message" string
                      "quid"       	quid_str
                      [ "documentation" text_value ]
                      "frequency"  	string
                      "synchronization" 	string
                      "dir"        	string
                      "sequence"   	string
                      "ordinal"    	number
                      "quidu"      	quid_str
                      "creation"   	bool
								    ")"
   */
  def messageParser: Parser[Message] = "(object" ~> "Message" ~> stringLiteral ~
                                          ("quid" ~> quid) ~
                                          opt("documentation" ~> textValue) ~
                                          ("frequency" ~> stringLiteral) ~
                                          ("synchronization" ~> stringLiteral) ~
                                          ("dir" ~> stringLiteral) ~
                                          ("sequence" ~> stringLiteral) ~
                                          ("ordinal" ~> wholeNumber) ~
                                          ("quidu" ~> quid) ~
                                          ("creation" ~> bool) <~
                                        ")" ^^ {
    case n ~ q ~ doc ~ f ~ s ~ d ~ seq ~ o ~ qu ~ c =>
      Message(n, q,
              if (doc.isDefined) doc.get else null,
              f, s, d, seq, o.toInt, qu, c)
  }


  /*
  list_messages = "(list" "Messages"
                    { object_message }
                   ")"
   */
  def messageListParser: Parser[Seq[Message]] = "(list" ~> "Messages" ~>
                                                  rep(messageParser) <~
                                                 ")"

  /*
  object_link = "(object" "Link"
							    "quid"       	quid_str
							    "supplier"   	string
							    "quidu"      	quid_str
							    "messages"   	list_messages
								 ")"
   */
  def linkParser: Parser[Link] = "(object" ~> "Link" ~>
                                    ("quid" ~> quid) ~
                                    ("supplier" ~> stringLiteral) ~
                                    ("quidu" ~> quid) ~
                                    opt("messages" ~> messageListParser) <~
                                  ")" ^^ {
    case q ~ s ~ qu ~ m =>
      Link(q, s, qu,
           if (m.isDefined) m.get else Array[Message]())
  }

  /*
  list_link = "(list" "link_list"
                { object_link }
               ")"
   */
  def linkListParser: Parser[Seq[Link]] = "(list" ~> "link_list" ~>
                                              rep(linkParser) <~
                                            ")"

  /*
  object_object = "(object" "Object" string
                    "quid"       	quid_str
                    [ "collaborators" list_link ]
                    [ "class"      	string ]
                    "quidu"      	quid_str
                    "persistence" string
                    "creationObj" bool
                    "multi"      	bool
                  ")"
   */
  def objectParser: Parser[_Object] = "(object" ~> "Object" ~> stringLiteral ~
                                          ("quid" ~> quid) ~
                                          opt("collaborators" ~> linkListParser) ~
                                          opt("class" ~> stringLiteral) ~
                                          ("quidu" ~> quid) ~
                                          ("persistence" ~> stringLiteral) ~
                                          ("creationObj" ~> bool) ~
                                          ("multi" ~> bool) <~
                                        ")" ^^ {
    case name ~ q ~ col ~ c ~ qu ~ p ~ obj ~ m =>
      _Object(name, q,
              if (col.isDefined) col.get else Array[Link](),
              if (c.isDefined) c.get else null,
              qu, p, obj, m)
  }



  /*
  object_mechanism = "(object" "Mechanism" ref
                        logical_models 	list_unit_ref
                      ")"
   */
  def mechanismParser: Parser[Mechanism] = "(object" ~> "Mechanism" ~> ref ~
                                              ("logical_models" ~> unitReferenceListParser) <~
                                            ")" ^^ {
    case r ~ mod =>
      Mechanism(r.toInt, mod)
  }


  /*
  object_use_case = "(object" "UseCase" string
				              [ "attributes" 	list_attr_set ]
				              "quid"       	quid_str
				              [ "documentation" string ]
				              [ "stereotype" string ]
				              [ "realized_interfaces" list_realized_rel ]
				              [ "visible_modules" list_unit_ref ]
				              [ "logical_models" list_unit_ref ]
				              [ "logical_presentations" list_unit_ref ]
				             ")"
   */
  def useCaseParser: Parser[UseCase] = "(object" ~> "UseCase" ~> stringLiteral ~
                                          opt("attributes" ~> attrListParser) ~
                                          ("quid" ~> quid) ~
                                          opt("documentation" ~> textValue) ~
                                          opt("stereotype" ~> stringLiteral) ~
                                          opt("superclasses" ~> inheritanceRelationshipListParser) ~
                                          opt("realized_interfaces" ~> realizeRelationshipListParser) ~
                                          opt("visible_modules" ~> dependencyListParser) ~
                                          opt("logical_models" ~> unitReferenceListParser) ~
                                          opt("logical_presentations" ~> unitReferenceListParser) <~
                                        ")" ^^ {
    case name ~ attr ~ q ~ doc ~ s ~ sup ~ ri ~ vmod ~ lm ~ lp =>
      UseCase(name,
              q,
              if (doc.isDefined) doc.get else null,
              if (attr.isDefined) attr.get else Array[BaseAttribute](),
              if (s.isDefined) s.get else null,
              if (sup.isDefined) sup.get else Array[InheritanceRelationship](),
              if (ri.isDefined) ri.get else Array[RealizeRelationship](),
              if (vmod.isDefined) vmod.get else Array[DependencyRelationship](),
              if (lm.isDefined) lm.get else Array[UnitReference](),
              if (lp.isDefined) lp.get else Array[UnitReference]())
  }

  /*
  object_focus_of_control = "(object" "Focus_Of_Control" string ref
                                "location"   	point
                                "line_color" 	number
                                "InterObjView" 	ref
                                "height"     	number
                                "y_coord"    	number
                                "Nested"     	bool
                              ")"
   */
  def focusOfControlParser: Parser[FocusOfControl] = "(object" ~> "Focus_Of_Control" ~> stringLiteral ~ ref ~
                                                        ("location" ~> pointParser) ~
                                                        ("line_color" ~> wholeNumber) ~
                                                        ("InterObjView" ~> ref) ~
                                                        ("height" ~> wholeNumber) ~
                                                        ("y_coord" ~> wholeNumber) ~
                                                        ("Nested" ~> bool) <~
                                                      ")" ^^ {
    case name ~ ref ~ loc ~ col ~ iov ~ h ~ y ~ n =>
      FocusOfControl(name, ref.toInt,
                     loc, col.toInt,
                     iov.toInt,
                     h.toInt,
                     y.toInt,
                     n)
  }



  /*
  object_inter_obj_view = "(object" "InterObjView" string ref
                            "location"   	point
                            "font"       	object_font
                            "label"      	object_item_label
                            "icon"       	string
                            "icon_style" 	string
                            "line_color" 	number
                            "fill_color" 	number
                            "quidu"      	quid_str
                            "width"      	number
                            "height"     	number
                            "icon_height" number
                            "icon_width" 	number
                            "icon_y_offset"	number
                            [ "annotation" 	number ]
                            { "Focus_Of_Control" 	object_focus_of_control }
                         ")"
   */
  def interObjViewParser: Parser[InterObjView] = "(object" ~> "InterObjView" ~> stringLiteral ~ ref ~
                                                    ("location" ~> pointParser) ~
                                                    ("font" ~> fontParser) ~
                                                    ("label" ~> itemLabelParser) ~
                                                    opt("icon" ~> stringLiteral) ~
                                                    ("icon_style" ~> stringLiteral) ~
                                                    ("line_color" ~> wholeNumber) ~
                                                    ("fill_color" ~> wholeNumber) ~
                                                    ("quidu" ~> quid) ~
                                                    ("width" ~> wholeNumber) ~
                                                    ("height" ~> wholeNumber) ~
                                                    ("icon_height" ~> wholeNumber) ~
                                                    ("icon_width" ~> wholeNumber) ~
                                                    ("icon_y_offset" ~> wholeNumber) ~
                                                    opt("annotation" ~> wholeNumber) ~
                                                    rep("Focus_Of_Control" ~> focusOfControlParser) <~
                                                  ")" ^^ {
    case name ~ r ~ l ~ f ~ lab ~ i ~ is ~ lc ~ fc ~ qu ~ w ~ h ~ ih ~ iw ~ y ~ a ~ foc =>
      InterObjView(name, r.toInt,
                   l, f, lab,
                   if (i.isDefined) i.get else null,
                   is,
                   lc.toInt,
                   fc.toInt,
                   qu,
                   w.toInt, h.toInt,
                   ih.toInt, iw.toInt,
                   w.toInt,
                   if (a.isDefined) a.get.toInt else -1,
                   foc)
  }


  /*
  object_interaction_diagram = "(object" "InteractionDiagram" string
                                  "mechanism_ref" 	ref
                                  "quid"       	quid_str
                                  "title"      	string
                                  [ "documentation" text_value ]
                                  "zoom"       	number
                                  "max_height" 	number
                                  "max_width"  	number
                                  "origin_x"   	number
                                  "origin_y"   	number
                                  "items"      	list_diagram_item
                                ")"
   */
  def interactionDiagramParser: Parser[InteractionDiagram] =
    "(object" ~> "InteractionDiagram" ~> stringLiteral ~
        ("mechanism_ref" ~> ref) ~
        ("quid" ~> quid) ~
        ("title" ~> stringLiteral) ~
        opt("documentation" ~> textValue) ~
        ("zoom" ~> wholeNumber) ~
        ("max_height" ~> wholeNumber) ~
        ("max_width" ~> wholeNumber) ~
        ("origin_x" ~> wholeNumber) ~
        ("origin_y" ~> wholeNumber) ~
        ("items" ~> diagramItemListParser) <~
      ")" ^^ {
    case n ~ m ~ q ~ t ~ doc ~ z ~ h ~ w ~ x ~ y ~ items =>
      InteractionDiagram(n, m.toInt,
                         q, t,
                         if (doc.isDefined) doc.get else null,
                         z.toInt,
                         h.toInt, w.toInt,
                         x.toInt, y.toInt,
                         items)
  }

  /*
  object_class_utility = "(object" "Class_Utility" string
                            "quid"       	    quid_str
                            "stereotype" 	    string
                            "external_docs" 	list_external_doc
                          ")"
   */
  def classUtilityParser: Parser[ClassUtility] = "(object" ~> "Class_Utility" ~> stringLiteral ~
                                                    ("quid" ~> quid) ~
                                                    ("stereotype" ~> stringLiteral) ~
                                                    ("external_docs" ~> externalDocListParser) <~
                                                  ")" ^^ {
    case n ~ q ~ s ~ ext => ClassUtility(n, q, s, ext)
  }

  /*
  list_unit_ref = "(list" "unit_reference_list"
                    {
                      object_class
                      | object_class_category
                      | object_class_category_ref
                      | object_use_case_diagram
                      | object_assoc
                      | object_class_diagram
                      | object_module
                      | object_module_diagram
                      | object_use_case
                      | object_object
                      | object_mechanism
                      | object_interaction_diagram
                      | object_class_utility
                    }
                   ")"
  */
  def unitReferenceListParser: Parser[Seq[UnitReference]] = "(list" ~> "unit_reference_list" ~>
                                                              rep(classParser
                                                                |classCategoryRefParser
                                                                |classCategoryParser
                                                                |useCaseDiagramParser
                                                                |associationParser
                                                                |classDiagramParser
                                                                |moduleParser
                                                                |moduleDiagramParser
                                                                |useCaseParser
                                                                |objectParser
                                                                |mechanismParser
                                                                |interactionDiagramParser
                                                                |classUtilityParser) <~
                                                              ")"


  /*
    object_module_diagram = "(object" "Module_Diagram" string
                            "quad" quad_str
                            "title" string
                            "zoom" number
                            "max_height" number
                            "max_width" number
                            "origin_x" number
                            "origin_y" number
                            "items" list_diagram_item ")"
  */
  def moduleDiagramParser: Parser[ModuleDiagram] = "(object" ~> "Module_Diagram" ~> stringLiteral ~
                                                    "quid" ~ quid ~
                                                    "title" ~ stringLiteral ~
                                                    "zoom" ~ wholeNumber ~
                                                    "max_height" ~ wholeNumber ~
                                                    "max_width" ~ wholeNumber ~
                                                    "origin_x" ~ wholeNumber ~
                                                    "origin_y" ~ wholeNumber ~
                                                    "items" ~ diagramItemListParser <~
                                                    ")" ^^ {
    case name ~
        "quid" ~ q ~
        "title" ~ t ~
        "zoom" ~ z ~
        "max_height" ~ h ~
        "max_width" ~ w ~
        "origin_x" ~ x ~
        "origin_y" ~ y ~
        "items" ~ it => ModuleDiagram(name,
                                      q,
                                      t,
                                      z.toInt,
                                      h.toInt, w.toInt,
                                      x.toInt, y.toInt,
                                      it)
  }


  /*
    object_module = "(object" "module" string string string
                        "quid" quid_str
                        "stereotype" string
                        "language" string
                        "class" string
                        "quidu" quid_str
                        "class" string
                        "quidu" quid_str
                    ")"
  */
  def moduleParser: Parser[Module] = "(object" ~> "module" ~> stringLiteral ~ stringLiteral ~ stringLiteral ~
                                      "quid" ~ quid ~
                                      "stereotype" ~ stringLiteral ~
                                      "language" ~ stringLiteral ~
                                      "class" ~ stringLiteral ~
                                      "quidu" ~ quid ~
                                      "class" ~ stringLiteral ~
                                      "quidu" ~ quid <~
                                      ")" ^^ {
    case name ~ modType ~ modPart ~
        "quid" ~ q ~
        "stereotype" ~ st ~
        "language" ~ lang ~
        "class" ~ cls ~
        "quidu" ~ qu ~
        "class" ~ cls2 ~
        "quidu" ~ qu2 => Module(name, modType, modPart,
                                q,
                                st,
                                lang,
                                cls,
                                qu,
                                cls2,
                                qu2)
  }

/*
  object_inheritance_relationship = "(object" "Inheritance_Relationship"
                                      "quid" quid_str
                                      [ "label" string ]
                                      "supplier" string
                                      "quidu" quid_str
                                     ")"
*/
  def inheritanceRelationshipParser: Parser[InheritanceRelationship] = "(object" ~> "Inheritance_Relationship" ~>
                                                                          "quid" ~ quid ~
                                                                          opt("label" ~> stringLiteral) ~
                                                                          "supplier" ~ stringLiteral ~
                                                                          "quidu" ~ quid <~
                                                                        ")" ^^ {
  case "quid" ~ q ~
        lbl ~
        "supplier" ~ s ~
        "quidu" ~ qu => InheritanceRelationship(q,
                                                if (lbl.isDefined) lbl.get else null,
                                                s,
                                                qu)
  }

/*
  list_inheritance_relationship = "(list" "inheritance_relationship_list"
                                    { object_inheritance_relationship }
                                   ")"
*/
  def inheritanceRelationshipListParser: Parser[Seq[InheritanceRelationship]] =
                        "(list" ~> "inheritance_relationship_list" ~>
                          rep(inheritanceRelationshipParser) <~
                        ")"

/*
  object_realize_relationship = "(object" "Realize_Relationship"
                                  "quid" quid_str
                                  "supplier" string
                                  "quidu" quid_str
                                 ")"
*/
  def realizeRelationshipParser = "(object" ~> "Realize_Relationship" ~>
                                    "quid" ~ quid ~
                                    opt("label" ~> stringLiteral) ~
                                    "supplier" ~ stringLiteral ~
                                    "quidu" ~ quid <~
                                    ")" ^^ {
  case "quid" ~ q ~
        lbl ~
        "supplier" ~ s ~
        "quidu" ~ qu => RealizeRelationship(q,
                                            if (lbl.isDefined) lbl.get else null,
                                            s, qu)
  }

/*
  list_realized_rel = "(list" "realize_rel_list"
                        { object_realize_relationship }
                      ")"
*/
  def realizeRelationshipListParser: Parser[Seq[RealizeRelationship]] = "(list" ~> "realize_rel_list" ~>
    rep(realizeRelationshipParser) <~
  ")"

/*
  object_class_attribute = "(object" "ClassAttribute" string
                              "quid" quid_str
                              [ "documentation" text_value ]
                              [ "stereotype" string ]
                              [ "type" text_value ]
                              [ "quidu" string ]
                              [ "initv" string ]
                              [ "exportControl" string ]
                              [ "derived" bool ]
                            ")"
*/
  def classAttributeParser: Parser[ClassAttribute] = "(object" ~> "ClassAttribute" ~> stringLiteral ~
                                                        "quid" ~ quid ~
                                                        opt("documentation" ~> textValue) ~
                                                        opt("stereotype" ~> stringLiteral) ~
                                                        opt("type" ~> textValue) ~
                                                        opt("quidu" ~> quid) ~
                                                        opt("initv" ~> stringLiteral) ~
                                                        opt("exportControl" ~> stringLiteral) ~
                                                        opt("derived" ~> bool) <~
                                                      ")" ^^ {
  case name ~
      "quid" ~ q ~
      doc ~
      stereotype ~
      tp ~
      qu ~
      init ~
      exp ~ d => ClassAttribute(name,
                            q,
                            if (doc.isDefined) doc.get else null,
                            if (stereotype.isDefined) stereotype.get else null,
                            if (tp.isDefined) tp.get else null,
                            if (qu.isDefined) qu.get else null,
                            if (init.isDefined) init.get else null,
                            if (exp.isDefined) exp.get else null,
                            if (d.isDefined) d.get else false)
  }

/*
  list_class_attributes = "(list" "class_attribute_list"
                            { object_class_attribute }
                           ")"
*/
  def classAttributeListParser: Parser[Seq[ClassAttribute]] = "(list" ~> "class_attribute_list" ~>
    rep(classAttributeParser) <~
  ")"

  /*
  object_external_doc = "(object" "external_doc"
                          "external_doc_path" string
                          ")"
   */
  def externalDocParser: Parser[ExternalDoc] = "(object" ~> "external_doc" ~>
                                                  "external_doc_path" ~> stringLiteral <~
                                                ")" ^^ {
    case path => ExternalDoc(path)
  }

  /*
  list_external_doc = "(list" "external_doc_list"
                        { object_external_doc }
                       ")"
   */
  def externalDocListParser: Parser[Seq[ExternalDoc]] = "(list" ~> "external_doc_list" ~>
                                                            rep(externalDocParser) <~
                                                          ")"

  /*
  list_nested_classes = "(list" "nestedClasses" { object_class } ")"
   */
  def nestedClassesListParser: Parser[Seq[Class]] = "(list" ~> "nestedClasses" ~>
                                                        rep(classParser) <~
                                                      ")"

  /*
    object_class = "(object" "Class" string
                      "quid" quid_str
                      [ "documentation" text_value ]
                      [ "stereotype" string ]
                      [ "external_docs" list_external_doc ]
                      [ "exportControl" string ]
                      [ "superclasses" list_inheritance_relationship ]
                      [ "visible_modules" list_dependency ]
                      [ "realized_interfaces" list_realized_interfaces ]
                      [ "class_attributes" list_class_attributes ]
                      [ "used_nodes" list_uses_rel ]
                      [ "operations" list_operations ]
                      [ "class_attributes" list_class_attributes ]
                      [ "module" string ]
                      [ "quidu" quid ]
                      [ "statemachine" object_state_machine ]
                      [ "persistence" string ]
                      [ "cardinality" cardinality_def ]
                      [ "abstract" bool ]
                    ")"
  */
  def classParser: Parser[Class] = "(object" ~> "Class" ~> stringLiteral ~
                                    "quid" ~ quid ~
                                    opt("documentation" ~> textValue) ~
                                    opt("stereotype" ~> stringLiteral) ~
                                    opt("external_docs" ~> externalDocListParser) ~
                                    opt("exportControl" ~> stringLiteral) ~
                                    opt("superclasses" ~> inheritanceRelationshipListParser) ~
                                    opt("visible_modules" ~> dependencyListParser) ~
                                    opt("used_nodes" ~> usesRelationshipListParser) ~
                                    opt("realized_interfaces" ~> realizeRelationshipListParser) ~
                                    opt("operations" ~> operationListParser) ~
                                    opt("class_attributes" ~> classAttributeListParser) ~
                                    opt("module" ~> stringLiteral) ~
                                    opt("quidu" ~> quid) ~
                                    opt("statemachine" ~> stateMachineParser) ~
                                    opt("nestedClasses" ~> nestedClassesListParser) ~
                                    opt("abstract" ~> bool) ~
                                    opt("persistence" ~> stringLiteral) ~
                                    opt("cardinality" ~> cardinalityParser) <~
                                    ")" ^^ {
    case name ~
        "quid" ~ q ~
        doc ~
        st ~
        extDoc ~
        ec ~
        sup ~
        vmod ~
        used ~
        ri ~
        opList ~
        attr ~
        mod ~
        qu ~
        stateMachine ~
        nest ~
        abs ~
        per ~
        card => Class(name,
                      q,
                      if (extDoc.isDefined) extDoc.get else null,
                      if (ec.isDefined) ec.get else null,
                      if (stateMachine.isDefined) stateMachine.get else null,
                      if (doc.isDefined) doc.get else null,
                      if (st.isDefined) st.get else null,
                      if (sup.isDefined) sup.get else Array[InheritanceRelationship](),
                      if (ri.isDefined) ri.get else Array[RealizeRelationship](),
                      if (attr.isDefined) attr.get else Array[ClassAttribute](),
                      if (opList.isDefined) opList.get else null,
                      if (vmod.isDefined) vmod.get else null,
                      if (mod.isDefined) mod.get else null,
                      if (qu.isDefined) qu.get else null,
                      if (used.isDefined) used.get else null,
                      if (nest.isDefined) nest.get else Array[Class](),
                      if (per.isDefined) per.get else null,
                      if (card.isDefined) card.get else null,
                      if (abs.isDefined) abs.get else false)
  }

  /*
    object_class_diagram = "(object" "ClassDiagram" string
                              "quid" quid_str
                              "title"string
                               [ "documentation" text_value ]
                              "zoom" number
                              "max_height" number
                              "max_width" number
                              "origin_x" number
                              "origin_y" number
                              "items" list_diagram_item
                           ")"
  */
  def classDiagramParser: Parser[ClassDiagram] = "(object" ~> "ClassDiagram" ~> stringLiteral ~
                                                  "quid" ~ quid ~
                                                  "title" ~ stringLiteral ~
                                                  opt("documentation" ~> textValue) ~
                                                  opt("external_docs" ~> externalDocListParser) ~
                                                  "zoom" ~ wholeNumber ~
                                                  "max_height" ~ wholeNumber ~
                                                  "max_width" ~ wholeNumber ~
                                                  "origin_x" ~ wholeNumber ~
                                                  "origin_y" ~ wholeNumber ~
                                                  "items" ~ diagramItemListParser <~
                                                  ")" ^^ {
    case name ~
        "quid" ~ q ~
        "title" ~ t ~
        doc ~
        ext ~
        "zoom" ~ z ~
        "max_height" ~ h ~
        "max_width" ~ w ~
        "origin_x" ~ x ~
        "origin_y" ~ y ~
        "items" ~ it => ClassDiagram(name,
                                      q,
                                      t,
                                      if (doc.isDefined) doc.get else null,
                                      if (ext.isDefined) ext.get else Array[ExternalDoc](),
                                      z.toInt,
                                      h.toInt, w.toInt,
                                      x.toInt, y.toInt,
                                      it)
  }




  /*
    object_compartment = "(object" "Compartment"
                            "Parent_View" ref
                            "location" point
                            "font" font
                            "icon_style" string
                            [ "fill_color" number ]
                            "anchor" number
                            "nlines" number
                            "max_width" number
                            "justify" number
                         ")"
  */
  def compartmentParser: Parser[Compartment] = "(object" ~> "Compartment" ~>
                                                "Parent_View" ~ ref ~
                                                "location" ~ pointParser ~
                                                "font" ~ fontParser ~
                                                "icon_style" ~ stringLiteral ~
                                                opt("fill_color" ~> wholeNumber) ~
                                                "anchor" ~ wholeNumber ~
                                                "nlines" ~ wholeNumber ~
                                                "max_width" ~ wholeNumber ~
                                                "justify" ~ wholeNumber <~
                                                ")" ^^ {
    case "Parent_View" ~ parent ~
          "location" ~ loc ~
          "font" ~ font ~
          "icon_style" ~ style ~
          fill ~
          "anchor" ~ an ~
          "nlines" ~ nl ~
          "max_width" ~ width ~
          "justify" ~ just => Compartment(parent.toInt,
                                          loc,
                                          font,
                                          style,
                                          if (fill.isDefined) fill.get.toInt else -1,
                                          an.toInt,
                                          nl.toInt,
                                          width.toInt,
                                          just.toInt)
  }

  /*

  list_point = "(list" "Points"
                 { point }
                ")"
 */
  def pointListParser: Parser[Seq[Point]] = "(list" ~> "Points" ~>
    rep(pointParser) <~
  ")"

  /*
  object_realize_view = "(object" "RealizeView" string ref
                          [ "font"  object_font ]
                          [ "label" object_seglabel ]
                          "stereotype" 	bool
                          "line_color" 	number
                          "quidu"      	quid_str
                          "client"     	ref
                          "supplier"   	ref
                          "vertices"   	list_point
                          "line_style" 	number
			                  ")"
   */


  def realizeViewParser: Parser[RealizeView] = ("(object" ~ "RealizeView") ~> stringLiteral ~ ref ~
                                                opt("font" ~> fontParser) ~
                                                opt("label" ~> segLabelParser) ~
                                                ("stereotype" ~> bool) ~
                                                ("line_color" ~> wholeNumber) ~
                                                ("quidu" ~> quid) ~
                                                ("client" ~> ref) ~
                                                ("supplier" ~> ref) ~
                                                ("vertices" ~> pointListParser) ~
                                                ("line_style" ~> wholeNumber) <~
                                              ")" ^^ {
    case name ~ refTo ~ f ~ lbl ~ ster ~ lineCol ~ q ~ client ~ sup ~ vert ~ lineSt =>
        RealizeView(name,
                    refTo.toInt,
                    if (f.isDefined) f.get else null,
                    if (lbl.isDefined) lbl.get else null,
                    ster,
                    lineCol.toInt,
                    q,
                    client.toInt,
                    sup.toInt,
                    vert,
                    lineSt.toInt)
  }

  /*
  object_assoc_constraint_view = "(object" "AssocConstraintView" string ref
                                    [ "font"      object_font ]
                                    "stereotype" 	bool
                                    "line_color" 	number
                                    "client"     	ref
                                    "supplier"   	ref
                                    "vertices"   	list_point
                                    "line_style" 	number
                                    [ "origin_attachment" 	  point ]
                                    [ "terminal_attachment" 	point ]
                                  ")"
   */
  def assocConstraintView: Parser[AssocConstraintView] = "(object" ~> "AssocConstraintView" ~> stringLiteral ~ ref ~
                                opt("font" ~> fontParser) ~
                                ("stereotype" ~> bool) ~
                                ("line_color" ~> wholeNumber) ~
                                ("client" ~> ref) ~
                                ("supplier" ~> ref) ~
                                ("vertices" ~> pointListParser) ~
                                ("line_style" ~> wholeNumber) ~
                                opt("origin_attachment" ~> pointParser) ~
                                opt("terminal_attachment" ~> pointParser) <~
                              ")" ^^ {
    case n ~ r ~ f ~ st ~ lc ~ c ~ s ~ v ~ ls ~ oa ~ ta =>
      AssocConstraintView(n, r.toInt,
                          if (f.isDefined) f.get else null,
                          st,
                          lc.toInt,
                          c.toInt, s.toInt,
                          v, ls.toInt,
                          if (oa.isDefined) oa.get else null,
                          if (ta.isDefined) ta.get else null)
  }


  /*
    object_class_view = "(object" "ClassView" string string ref
                            [ "ShowCompartmentStereotypes" bool ]
                            [ "ShowOperationSignature" bool ]
                            [ "IncludeAttribute" bool ]
                            [ "IncludeOperation" bool ]
                            [ "SuppressAttribute" bool ]
                            [ "SuppressOperation" bool ]
                            "location" point
                            "font" object_font
                            "label" object_item_label
                            [ "stereotype" object_item_label ]
                            [ "icon" string ]
                            "icon_style" string
                            "line_color" number
                            "fill_color" number
                            "quidu" quid_str
                            [ "compartment" object_compartment ]
                            [ "width" number ]
                            [ "height" number ]
                            [ "annotation" number ]
                            [ "autoResize" bool ]
                        ")"
  */
  def classViewParser: Parser[ClassView] = "(object" ~> "ClassView" ~> stringLiteral ~ stringLiteral ~ ref ~
                                              opt("ShowCompartmentStereotypes" ~> bool) ~
                                              opt("SuppressAttribute" ~> bool) ~
                                              opt("SuppressOperation" ~> bool) ~
                                              opt("IncludeAttribute" ~> bool) ~
                                              opt("IncludeOperation" ~> bool) ~
                                              opt("ShowOperationSignature" ~> bool) ~
                                              "location" ~ pointParser ~
                                              "font" ~ fontParser ~
                                              "label" ~ itemLabelParser ~
                                              opt("stereotype" ~> itemLabelParser) ~
                                              opt("icon" ~> stringLiteral) ~
                                              "icon_style" ~ stringLiteral ~
                                              "line_color" ~ wholeNumber ~
                                              opt("fill_color" ~> wholeNumber) ~
                                              "quidu" ~ quid ~
                                              opt("compartment" ~> compartmentParser) ~
                                              opt("width" ~> wholeNumber) ~
                                              opt("height" ~> wholeNumber) ~
                                              opt("annotation" ~> wholeNumber) ~
                                              opt("autoResize" ~> bool) <~
                                            ")" ^^ {
    case typ ~ nm ~ rf ~
        compSter ~
        supAttr ~
        supOp ~
        incAttr ~
        incOp ~
        sign ~
        "location" ~ loc ~
        "font" ~ fnt ~
        "label" ~ lab ~
        ster ~
        ic ~
        "icon_style" ~ style ~
        "line_color" ~ lineClr ~
        fillClr ~
        "quidu" ~ q ~
        compart ~
        w ~
        h ~
        an ~
      res =>
          val v = new ClassView

          v.`type` = typ
          v.name = nm
          v.refTo = rf.toInt
          v.showCompartmentStereotypes = if (compSter.isDefined) compSter.get else false
          v.showOperationSignature = if (sign.isDefined) sign.get else false
          v.suppressAttribute = if (supAttr.isDefined) supAttr.get else false
          v.suppressOperation = if (supOp.isDefined) supOp.get else false
          v.includeAttribute = if (incAttr.isDefined) incAttr.get else false
          v.includeOperation = if (incOp.isDefined) incOp.get else false
          v.location = loc
          v.font = fnt
          v.label = lab
          v.stereotype = if (ster.isDefined) ster.get else null
          v.icon = if (ic.isDefined) ic.get else null
          v.iconStyle = style
          v.lineColor = lineClr.toInt
          v.fillColor = if (fillClr.isDefined) fillClr.get.toInt else -1
          v.quidu = q
          v.compartment = if (compart.isDefined) compart.get else null
          v.width = if (w.isDefined) w.get.toInt else -1
          v.height = if (h.isDefined) h.get.toInt else -1
          v.annotation = if (an.isDefined) an.get.toInt else -1
          v.autoResize = if (res.isDefined) res.get else false
          v
  }


      /*
        object_assoc = "(object" "Association" string
                          "quid" quid_str
                          [ "stereotype" string ]
                          "roles" list_role
                        ")"
      */

  def associationParser: Parser[Association] = "(object" ~> "Association" ~> stringLiteral ~
                                                  ("quid" ~> quid) ~
                                                  opt("stereotype" ~> stringLiteral) ~
                                                  opt("documentation" ~> textValue) ~
                                                  ("roles" ~> roleListParser) <~
                                                ")" ^^ {
    case name ~
        q ~
        st ~
        doc ~
        roles => Association(name,
                              q,
                              if (st.isDefined) st.get else null,
                              if (doc.isDefined) doc.get else null,
                              roles)
  }


  /*
    list_role = "(list" "role_list" { object_role } ")"
  */
  def roleListParser: Parser[Seq[Role]] = "(list" ~> "role_list" ~>
                                              rep(roleParser) <~
                                           ")"


  /*
    object_role = "(object" "Role" string
                      "quid" quid_str
                      [ "label" text_value ]
                      "supplier" string
                      "quidu" quid_str
                      [ "exportControl" string ]
                      [ "Containment" string ]
                      [ "client_cardinality" cardinality_def ]
                      [ "is_navigable" bool ]
                      [ "is_aggregate" bool ]
                  ")"
  */
  def roleParser: Parser[Role] = "(object" ~> "Role" ~> stringLiteral ~
                                  "quid" ~ quid ~
                                  opt("label" ~> textValue) ~
                                  "supplier" ~ stringLiteral ~
                                  "quidu" ~ quid ~
                                  opt("exportControl" ~> stringLiteral) ~
                                  opt("client_cardinality" ~> cardinalityParser) ~
                                  opt("Containment" ~> stringLiteral) ~
                                  opt("is_navigable" ~> bool) ~
                                  opt("is_aggregate" ~> bool) <~
                                  ")" ^^ {
    case name ~
          "quid" ~ q ~
          label ~
          "supplier" ~ supplier ~
          "quidu" ~ qu ~
          exp ~
          card ~
          cont ~
          nav ~
          aggr => Role(name,
                      q,
                      if (label.isDefined) label.get else null,
                      supplier,
                      qu,
                      if (exp.isDefined) exp.get else null,
                      if (cont.isDefined) cont.get else null,
                      if (aggr.isDefined) aggr.get else false,
                      if (card.isDefined) card.get else null,
                      if (nav.isDefined) nav.get else false)
  }

  /*
    cardinality_def = "(value cardinality" string ")"
  */
  def cardinalityParser: Parser[String] = "(value" ~> ("cardinality"|"Cardinality") ~> stringLiteral <~ ")"


  /*
    list_operations = "(list" "Operations" { object_operation } ")"
  */
  def operationListParser: Parser[Seq[Operation]] = "(list" ~> "Operations" ~>
                                                        rep(operationParser) <~
                                                     ")"

  /*
    object_operation = "(object" "Operation" string
                           "quid" quid_str
                           [ "documentation" text_value ]
                           [ "external_docs" list_external_doc ]
                           [ "parameters" list_parameters ]
                           [ "result" string ]
                           [ "stereotype" string ]
                           "concurrency" string
                           [ "abstract" bool ]
                           "opExportControl" string
                           "uid" number
                           [ "quidu" quid_str ]
                           [ "statemachine" object_state_machine ]
                        ")"
  */
  def operationParser: Parser[Operation] = "(object" ~> "Operation" ~> stringLiteral ~
                                              ("quid" ~> quid) ~
                                              opt("documentation" ~> textValue) ~
                                              opt("external_docs" ~> externalDocListParser) ~
                                              opt("parameters" ~> parametersListParser) ~
                                              opt("result" ~> stringLiteral) ~
                                              opt("stereotype" ~> stringLiteral) ~
                                              ("concurrency" ~> stringLiteral) ~
                                              opt("abstract" ~> bool) ~
                                              ("opExportControl" ~> stringLiteral) ~
                                              ("uid" ~> wholeNumber) ~
                                              opt("quidu" ~> quid) ~
                                              opt("statemachine" ~> stateMachineParser) <~
                                            ")" ^^ {
    case name ~
        q ~
        doc ~
        ext ~
        params ~
        res ~
        st ~
        conc ~
        abs ~
        exp ~
        uid ~
        qu ~
        sm => Operation(name,
                        q,
                        if (doc.isDefined) doc.get else null,
                        if (ext.isDefined) ext.get else Array[ExternalDoc](),
                        if (params.isDefined) params.get else Array[Parameter](),
                        if (res.isDefined) res.get else null,
                        if (st.isDefined) st.get else null,
                        conc,
                        if (abs.isDefined) abs.get else false,
                        exp,
                        uid.toInt,
                        if (qu.isDefined) qu.get else null,
                        if (sm.isDefined) sm.get else null)
  }

  /*
  object_role_view = "(object" "RoleView" string ref
                        "Parent_View" ref
                        "location"   	point
                        [ "font"      object_font ]
                        [ "label"     object_seglabel ] # WTF?
                        "stereotype" 	bool
                        "line_color" 	number
                        "quidu"      	quid_str
                        "client"     	ref
                        "supplier"   	ref
                        "vertices"   	list_point
                        "line_style" 	number
                        [ "origin_attachment" 	  point ]
				                [ "terminal_attachment" 	point ]
                        [ "label"     object_seglabel ] # WFT???
                      ")"
   */
  def roleViewParser: Parser[RoleView] = "(object" ~> "RoleView" ~> stringLiteral ~ ref ~
                                            ("Parent_View" ~> ref) ~
                                            ("location" ~> pointParser) ~
                                            opt("font" ~> fontParser) ~
                                            opt("label" ~> segLabelParser) ~
                                            ("stereotype" ~> bool) ~
                                            ("line_color" ~> wholeNumber) ~
                                            ("quidu" ~> quid) ~
                                            ("client" ~> ref) ~
                                            ("supplier" ~> ref) ~
                                            ("vertices" ~> pointListParser) ~
                                            ("line_style" ~> wholeNumber) ~
                                            opt("origin_attachment" ~> pointParser) ~
                                            opt("terminal_attachment" ~> pointParser) ~
                                            opt("label" ~> segLabelParser) <~
                                          ")" ^^ {
    case name ~ refTo ~ parent ~ loc ~ f ~ lab ~ st ~ line ~ qu ~ c ~ s ~ vert ~ style ~ oa ~ ta ~ lab2 =>
      RoleView(name, refTo.toInt,
               parent.toInt,
               loc,
               if (f.isDefined) f.get else null,
               if (lab.isDefined) lab.get else null,
               st,
               line.toInt,
               qu,
               c.toInt,
               s.toInt,
               vert,
               style.toInt,
               if (oa.isDefined) oa.get else null,
               if (ta.isDefined) ta.get else null,
               if (lab2.isDefined) lab2.get else null)
  }


  /*
  object_dependency_view = "(object" "DependencyView" string ref
                              [ "font"      object_font ]
                              [ "label"     object_item_label ]
                              "stereotype" 	bool
                              "line_color" 	number
                              "quidu"      	quid_str
                              "client"     	ref
                              "supplier"   	ref
                              "vertices"   	list_point
                              "line_style" 	number
                            ")"
 */
  def dependencyViewParser: Parser[DependencyView] =
  "(object" ~> "DependencyView" ~> stringLiteral ~ ref ~
      opt("font" ~> fontParser) ~
      opt("label" ~> itemLabelParser) ~
      ("stereotype" ~> bool) ~
      ("line_color" ~> wholeNumber) ~
      ("quidu" ~> quid) ~
      ("client" ~> ref) ~
      ("supplier" ~> ref) ~
      ("vertices" ~> pointListParser) ~
      ("line_style" ~> wholeNumber) <~
    ")" ^^ {
    case n ~ r ~ f ~ lbl ~ st ~ lc ~ qu ~ c ~ s ~ v ~ ls =>
      DependencyView(n, r.toInt,
        if (f.isDefined) f.get else null,
        if (lbl.isDefined) lbl.get else null,
        st, lc.toInt, qu, c.toInt, s.toInt, v, ls.toInt)
  }



  /*
  list_role_views = "(list" "RoleViews"
                      { object_role_view }
						        ")"
   */
  def roleViewListParser: Parser[Seq[RoleView]] = "(list" ~> "RoleViews" ~>
                                                    rep(roleViewParser) <~
                                                   ")"

  /*
  object_assoc_view_new = "(object" "AssociationViewNew" string ref
                            "location"   	point
                            [ "font"       	object_font ]
                            [ "label"       object_seglabel ]
                            "stereotype" 	bool|object_seglabel
                            "line_color" 	number
                            "quidu"      	quid_str
                            "roleview_list"	  list_role_views
                          ")"
   */
  def assocViewNewParser: Parser[AssociationViewNew] = "(object" ~> "AssociationViewNew" ~> stringLiteral ~ ref ~
                                                          ("location" ~> pointParser) ~
                                                          opt("font" ~> fontParser) ~
                                                          opt("label" ~> segLabelParser) ~
                                                          ("stereotype" ~> (bool|segLabelParser)) ~
                                                          ("line_color" ~> wholeNumber) ~
                                                          ("quidu" ~> quid) ~
                                                          ("roleview_list" ~> roleViewListParser) <~
                                                        ")" ^^ {
    case name ~ refTo ~ loc ~ f ~ lab ~ st ~ col ~ qu ~ list =>
      AssociationViewNew(name, refTo.toInt, loc,
                         if (f.isDefined) f.get else null,
                         if (lab.isDefined) lab.get else null,
                         st, col.toInt, qu, list)
  }


  /*
    list_parameters = "(list" "Parameters" { object_parameter } ")"
  */
  def parametersListParser: Parser[Seq[Parameter]] = "(list" ~> "Parameters" ~>
                                                        rep(parameterParser) <~
                                                      ")"

  /*
    object_parameter = "(object" "Parameter" string
                          "quid" quid_str
                          [ "documentation" text_value ]
                          "type" string
                          "quidu" quid_str
                        ")"
  */
  def parameterParser: Parser[Parameter] = "(object" ~> "Parameter" ~> stringLiteral ~
                                            "quid" ~ quid ~
                                            opt("documentation" ~> textValue) ~
                                            opt("type" ~> stringLiteral) ~
                                            opt("quidu" ~> stringLiteral) <~
                                            ")" ^^ {
    case name ~
        "quid" ~ q ~
        doc ~
        t ~
        u => new Parameter(name, q,
                            if (doc.isDefined) doc.get else null,
                            if (t.isDefined) t.get else null,
                            if (u.isDefined) u.get else null)
  }


  /*
    list_dependency = "(list" "dependency_list" { object_dependency_rel } ")"
  */
  def dependencyListParser: Parser[Seq[DependencyRelationship]] =
    "(list" ~> "dependency_list" ~>
      rep(dependencyRelationshipParser) <~
    ")"



  /*
    object_dependency_rel = "(object" "Dependency_Relationship"
                                "quid" quid_str
                                [ "label" string ]
                                "supplier" string
                                "quidu" quid_str
                            ")"
  */
  def dependencyRelationshipParser: Parser[DependencyRelationship] = "(object" ~> "Dependency_Relationship" ~>
                                                                      "quid" ~ quid ~
                                                                      opt("label" <~ stringLiteral) ~
                                                                      "supplier" ~ stringLiteral ~
                                                                      "quidu" ~ quid <~
                                                                      ")" ^^ {
    case "quid" ~ q ~
          label ~
          "supplier" ~ sup ~
          "quidu" ~ qu => DependencyRelationship(q,
                                                  if (label.isDefined) label.get else null,
                                                  sup,
                                                  qu)
  }


  /*
    list_uses_rel = "(list" "uses_relationship_list" { object_uses_rel } ")"
  */
  def usesRelationshipListParser: Parser[Seq[UsesRelationship]] =
    "(list" ~> "uses_relationship_list" ~>
       rep(usesRelationshipParser) <~
     ")"

  /*
    object_uses_rel = "(object" "Uses_Relationship"
                          "quid" quid_str
                          [ "stereotype" string ]
                          [ "label" string ]
                          "supplier" string
                          "quidu" quid_str
                      ")"
  */
  def usesRelationshipParser: Parser[UsesRelationship] = "(object" ~> "Uses_Relationship" ~>
                                                          "quid" ~ quid ~
                                                          opt("stereotype" ~> stringLiteral) ~
                                                          opt("label" ~> stringLiteral) ~
                                                          "supplier" ~ stringLiteral ~
                                                          "quidu" ~ quid <~
                                                          ")" ^^ {
    case "quid" ~ q ~
          st ~
          lbl ~
          "supplier" ~ sup ~
          "quidu" ~ qu => UsesRelationship(q,
                                            if (st.isDefined) st.get else null,
                                            if (lbl.isDefined) lbl.get else null,
                                            sup,
                                            qu)
  }

  /*
    object_use_case_diagram = "(object" "UseCaseDiagram" string
                                  "quid" quid_str
                                  "title" string
                                  [ "documentation" string ]
                                  "zoom" number
                                  "max_height" number
                                  "max_width" number
                                  "origin_x" number
                                  "origin_y" number
                                  "items" list_diagram_item
                              ")"
  */
  def useCaseDiagramParser: Parser[UseCaseDiagram] = "(object" ~> "UseCaseDiagram" ~> stringLiteral ~
                                                      "quid" ~ quid ~
                                                      "title" ~ stringLiteral ~
                                                      opt("documentation" ~> stringLiteral) ~
                                                      "zoom" ~ wholeNumber ~
                                                      "max_height" ~ wholeNumber ~
                                                      "max_width" ~ wholeNumber ~
                                                      "origin_x" ~ wholeNumber ~
                                                      "origin_y" ~ wholeNumber ~
                                                      "items" ~ diagramItemListParser <~
                                                      ")" ^^ {
    case name ~
        "quid" ~ q ~
        "title" ~ t ~
        doc ~
        "zoom" ~ z ~
        "max_height" ~ h ~
        "max_width" ~ w ~
        "origin_x" ~ x ~
        "origin_y" ~ y ~
        "items" ~ it => UseCaseDiagram(name,
                                        q,
                                        t,
                                        if (doc.isDefined) doc.get else null,
                                        z.toInt,
                                        h.toInt, w.toInt,
                                        x.toInt, y.toInt,
                                        it)
  }

  /*
object_state = "(object" "State" string
                  quid       	quid_str
                  [ "documentation" text_value ]
                  type       	string
                ")"
 */
  def stateParser: Parser[State] = "(object" ~> "State" ~> stringLiteral ~
                                      ("quid" ~> quid) ~
                                      opt("documentation" ~> textValue) ~
                                      ("type" ~> stringLiteral) <~
                                    ")" ^^ {
    case name ~ q ~ d ~ t =>
      State(name, q,
            if (d.isDefined) d.get else null,
            t)
  }

  /*
  object_decision = "(object" "Decision" string
                      quid       	quid_str
                      [ "documentation" text_value ]
                    ")"
   */
  def decisionParser: Parser[Decision] = "(object" ~> "Decision" ~> stringLiteral ~
                                            ("quid" ~> quid) ~
                                            opt("documentation" ~> textValue) <~
                                          ")" ^^ {
    case name ~ q ~ d => Decision(name, q,
                                  if (d.isDefined) d.get else null)
  }

  /*
  object_action_time = "(object" "ActionTime"
                        "when"       	string
                        ")"
   */
  def actionTimeParser: Parser[ActionTime] =
    "(object" ~> "ActionTime" ~> "when" ~> stringLiteral <~ ")" ^^ {
        case when => ActionTime(when)
    }


  /*
  object_action = "(object" "action" string
                    "quid"       	quid_str
                    "ActionTime" 	object_action_time
                    ")"
   */
  def actionParser: Parser[Action] = "(object" ~> "action" ~> stringLiteral ~
                                        ("quid" ~> quid) ~
                                        ("ActionTime" ~> actionTimeParser) <~
                                      ")" ^^ {
    case n ~ q ~ at =>
      Action(n, q, at)
  }


  /*
  list_action = "(list" "action_list"
							    { object_action }
								 ")"
   */
  def actionListParser: Parser[Seq[Action]] = "(list" ~> "action_list" ~>
                                                  rep(actionParser) <~
                                               ")"


  /*
  object_activity_state = "(object" "ActivityState" string
                            "quid"       	quid_str
                            [ "stereotype"    string ]
                            [ "documentation" text_value ]
                            [ "external_docs" list_external_doc ]
                            [ "actions" list_action ]
                            [ "statemachine" object_state_machine ]
                           ")"
   */
  def activityStateParser = "(object" ~> "ActivityState" ~> stringLiteral ~
                              ("quid" ~> quid) ~
                              opt("stereotype" ~> stringLiteral) ~
                              opt("documentation" ~> textValue) ~
                              opt("external_docs" ~> externalDocListParser) ~
                              opt("actions" ~> actionListParser) ~
                              opt("statemachine" ~> stateMachineParser) <~
                            ")" ^^ {
    case name ~ q ~ st ~ d ~ extDoc ~ a ~ sm =>
      ActivityState(name, q,
                     if (st.isDefined) st.get else null,
                     if (d.isDefined) d.get else null,
                     if (extDoc.isDefined) extDoc.get else Array[ExternalDoc](),
                     if (a.isDefined) a.get else Array[Action](),
                     if (sm.isDefined) sm.get else null)
  }

  /*
  list_states = "(list" "States"
                    { object_state | object_decision | object_activity_state }
                ")"
   */
  def stateListParser: Parser[Seq[StateListItem]] = "(list" ~> "States" ~>
                                                      rep(stateParser
                                                            |decisionParser
                                                            |activityStateParser) <~
                                                     ")"

  /*
  object_partition = "(object" "Partition" string
                        "quid"       	quid_str
                        "persistence" string
                        "creationObj" bool
                        "multi"      	bool
                      ")"
   */
  def partitionParser: Parser[Partition] = "(object" ~> "Partition" ~> stringLiteral ~
                                              ("quid" ~> quid) ~
                                              ("persistence" ~> stringLiteral) ~
                                              ("creationObj" ~> bool) ~
                                              ("multi" ~> bool) <~
                                            ")" ^^ {
    case n ~ q ~ p ~ c ~ m =>
      Partition(n, q, p, c, m)
  }


  /*
  list_partitions = "(list" "Partitions"
                        { object_partition }
                     ")"
   */
  def partitionListParser: Parser[Seq[Partition]] = "(list" ~ "Partitions" ~>
                                                        rep(partitionParser) <~
                                                      ")"

  /*
  list_objects = "(list" "Objects" ")"
   */
  def objectListParser: Parser[Seq[RoseObject]] = "(list" ~ "Objects" ~ ")" ^^ { case _ => Array[RoseObject]() }

  /*
  object_send_event = "(object" "sendEvent"
                        "quid"       	quid_str
                       ")"
   */
  def sendEventParser: Parser[SendEvent] = "(object" ~> "sendEvent" ~>
                                                "quid" ~> quid <~
                                              ")" ^^ { case q => new SendEvent(q) }

  /*
  object_event = "(object" "Event" name
                    "quid"       	quid_str
                    [ "parameters"  string ]
                  ")"
   */
  def eventParser: Parser[Event] = "(object" ~> "Event" ~> stringLiteral ~
                                      ("quid" ~> quid) ~
                                      opt("parameters" ~> stringLiteral) <~
                                    ")" ^^ {
    case name ~ q ~ p =>
      Event(name, q,
            if (p.isDefined) p.get else null)
  }

  /*
  object_state_transition = "(object" "State_Transition"
					"quid"       	quid_str
					[ "documentation" text_value ]
					[ "label"       string ]
					"supplier"   	text_value
					"supplier_quidu" 	quid_str
					"client"     	    text_value
					"client_quidu" 	  quid_str
					[ "Event"      	  object_event ]
					[ "condition"     string ]
					"sendEvent"  	    object_send_event
					")"
   */
  def stateTransitionParser = "(object" ~> "State_Transition" ~>
                                ("quid" ~> quid) ~
                                opt("documentation" ~> textValue) ~
                                opt("label" ~> stringLiteral) ~
                                ("supplier" ~> textValue) ~
                                ("supplier_quidu" ~> quid) ~
                                ("client" ~> textValue) ~
                                ("client_quidu" ~> quid) ~
                                opt("Event" ~> eventParser) ~
                                opt("condition" ~> stringLiteral) ~
                                ("sendEvent" ~> sendEventParser) <~
                              ")" ^^ {
    case q ~
          doc ~
          lab ~
          sup ~
          supqu ~
          cl ~
          clqu ~
          evt ~
          cond ~
          send => StateTransition(q,
                                  doc match {
                                    case d: Some[String] => d.get
                                    case None => null
                                  },
                                  lab match {
                                    case s: Some[String] => s.get
                                    case None => null
                                  },
                                  sup,
                                  supqu,
                                  cl,
                                  clqu,
                                  if (evt.isDefined) evt.get else null,
                                  if (cond.isDefined) cond.get else null,
                                  send)
  }

  /*
  list_transition = "(list" "transition_list"
                        { object_state_transition }
                     ")"
   */
  def transitionListParser: Parser[Seq[StateTransition]] = "(list" ~> "transition_list" ~>
                                                              rep(stateTransitionParser) <~
                                                            ")"


  /*
    object_inherit_view = "(object" "InheritView" string ref
                              [ "font" object_font ]
                              [ "label" object_seglabel ]
                              "stereotype" 	bool
                              "line_color" 	number
                              "quidu"      	quid_str
                              "client"     	ref
                              "supplier"   	ref
                              "vertices"   	list_point
                              "line_style" 	number
                              [ "origin_attachment" 	  point ]
                              [ "terminal_attachment" 	point ]
                              [ "drawSupplier"          ref ]
                            ")"
   */
  def inheritViewParser: Parser[InheritView] = "(object" ~> "InheritView" ~> stringLiteral ~ ref ~
                                                  opt("font" ~> fontParser) ~
                                                  opt("label" ~> segLabelParser) ~
                                                  ("stereotype" ~> bool) ~
                                                  ("line_color" ~> wholeNumber) ~
                                                  ("quidu" ~> quid) ~
                                                  ("client" ~> ref) ~
                                                  ("supplier" ~> ref) ~
                                                  ("vertices" ~> pointListParser) ~
                                                  ("line_style" ~> wholeNumber) ~
                                                  opt("origin_attachment" ~> pointParser) ~
                                                  opt("terminal_attachment" ~> pointParser) ~
                                                  opt("drawSupplier" ~> ref) <~
                                                ")" ^^ {
    case name ~ refTo ~ f ~ lbl ~ st ~ clr ~ qu ~ c ~ s ~ vert ~ style ~ oa ~ ta ~ ds =>
      InheritView(name, refTo.toInt,
                  if (f.isDefined) f.get else null,
                  if (lbl.isDefined) lbl.get else null,
                  st, clr.toInt, qu, c.toInt, s.toInt, vert, style.toInt,
                  if (oa.isDefined) oa.get else null,
                  if (ta.isDefined) ta.get else null,
                  if (ds.isDefined) ds.get.toInt else -1)
  }



  /*
  list_objectflow = "(list" "objectflow_list" ")"
   */
  def objectFlowListParser[Seq[ObjectFlow]] = "(list" ~ "objectflow_list" ~ ")" ^^ { case _ => Array[ObjectFlow]() }

  /*
  object_activity_diagram = "(object" "ActivityDiagram" string
                              "quid"       	quid_str
                              "title"      	string
                              [ "documentation" text_value ]
                              "zoom"       	number
                              "max_height" 	number
                              "max_width"  	number
                              "origin_x"   	number
                              "origin_y"   	number
                              "items"      	list_diagram_item
                            ")"

   */
  def activityDiagramParser: Parser[ActivityDiagram] = "(object" ~> "ActivityDiagram" ~> stringLiteral ~
                                                          ("quid" ~> quid) ~
                                                          ("title" ~> stringLiteral) ~
                                                          opt("documentation" ~> textValue) ~
                                                          ("zoom" ~> wholeNumber) ~
                                                          ("max_height" ~> wholeNumber) ~
                                                          ("max_width" ~> wholeNumber) ~
                                                          ("origin_x" ~> wholeNumber) ~
                                                          ("origin_y" ~> wholeNumber) ~
                                                          ("items" ~> diagramItemListParser) <~
                                                        ")" ^^ {
    case name ~ q ~ t ~ d ~ z ~ h ~ w ~ x ~ y ~ items =>
      ActivityDiagram(name, q, t,
                      if(d.isDefined) d.get else null,
                      z.toInt, h.toInt, w.toInt, x.toInt, y.toInt, items)
  }


  /*
  object_state_diagram = "(object" "State_Diagram" string
                            "quid"       	quid_str
                            "title"      	string
                            "zoom"       	number
                            "max_height" 	number
                            "max_width"  	number
                            "origin_x"   	number
                            "origin_y"   	number
                            "items"      	list_diagram_item
                          ")"
   */
  def stateDiagramParser: Parser[StateDiagram] = "(object" ~> "State_Diagram" ~> stringLiteral ~
                                                    ("quid" ~> quid) ~
                                                    ("title" ~> stringLiteral) ~
                                                    ("zoom" ~> wholeNumber) ~
                                                    ("max_height" ~> wholeNumber) ~
                                                    ("max_width" ~> wholeNumber) ~
                                                    ("origin_x" ~> wholeNumber) ~
                                                    ("origin_y" ~> wholeNumber) ~
                                                    ("items" ~> diagramItemListParser) <~
                                                  ")" ^^ {
    case name ~ q ~ t ~ z ~ h ~ w ~ x ~ y ~ items =>
      StateDiagram(name, q, t, z.toInt, h.toInt, w.toInt, x.toInt, y.toInt, items)
  }



  /*
  list_state_diagram = "(list" "StateDiagrams"
                          { object_activity_diagram }
                        ")"
   */
  def stateDiagramListParser: Parser[Seq[StateDiagramListItem]] = "(list" ~> "StateDiagrams" ~>
                                                                      rep(activityDiagramParser
                                                                          |stateDiagramParser) <~
                                                                    ")"

  /*
  object_state_machine = "(object" "State_Machine" string
                            "quid"       	    quid_str
                            "states"     	    list_states
                            "partitions" 	    list_partitions
                            "objects"    	    list_objects
                            "transitions" 	  list_transition
                            "objectflows" 	  list_objectflow
                            "statediagrams" 	list_state_diagram
                        ")"
   */
  def stateMachineParser: Parser[StateMachine] = "(object" ~> "State_Machine" ~> stringLiteral ~
                                                    ("quid" ~> quid) ~
                                                    ("states" ~> stateListParser) ~
                                                    ("partitions" ~> partitionListParser) ~
                                                    ("objects" ~> objectListParser) ~
                                                    ("transitions" ~> transitionListParser) ~
                                                    ("objectflows" ~> objectFlowListParser) ~
                                                    ("statediagrams" ~> stateDiagramListParser) <~
                                                  ")" ^^ {
    case name ~ q ~ s ~ p ~ o ~ t ~ f ~ d =>
      StateMachine(name, q, s, p, o, t, f, d)
  }


  /*
  object_swimlane = "(object" "Swimlane" string ref
                      [ "location"    point ]
                      "line_color" 	number
                      [ "quidu" 	quid_str ]
                      "width"      	number
                      ")"
  */
  def swimlaneParser: Parser[Swimlane] = ("(object" ~ "Swimlane") ~> stringLiteral ~ ref ~
                                            opt("location" ~> pointParser) ~
                                            ("line_color" ~> wholeNumber) ~
                                            opt("quidu" ~> quid) ~
                                            ("width" ~> wholeNumber) <~
                                          ")" ^^ {
    case name ~ refTo ~ l ~ lineClr ~ qu ~ width =>
      Swimlane(name, refTo.toInt,
        if (l.isDefined) l.get else null,
        lineClr.toInt,
        if (qu.isDefined) qu.get else null,
        width.toInt)
  }

  /*
  object_seglabel = "(object" "SegLabel" ref
                        "Parent_View" 	ref
                        "location"   	point
                        "font"       	object_font
                        [ "quidu"       quid_str ]
                        [ "hidden"     	bool ]
                        [ "anchor"      number ]
                        [ "anchor_loc"  number ]
                        "nlines"     	number
                        "max_width"  	number
                        "justify"    	number
                        "label"      	text_value
                        "pctDist"    	float
                        "height"     	number
                        "orientation" number
                     ")"
   */
def segLabelParser: Parser[SegLabel] = "(object" ~> "SegLabel" ~> ref ~
                                          ("Parent_View" ~> ref) ~
                                          ("location" ~> pointParser) ~
                                          ("font" ~> fontParser) ~
                                          opt("quidu" ~> quid) ~
                                          opt("hidden" ~> bool) ~
                                          opt("anchor" ~> wholeNumber) ~
                                          opt("anchor_loc" ~> wholeNumber) ~
                                          ("nlines" ~> wholeNumber) ~
                                          ("max_width" ~> wholeNumber) ~
                                          ("justify" ~> wholeNumber) ~
                                          ("label" ~> textValue) ~
                                          ("pctDist" ~> floatingPointNumber) ~
                                          ("height" ~> wholeNumber) ~
                                          ("orientation" ~> wholeNumber) <~
                                        ")" ^^ {
    case refTo ~ par ~ loc ~ fnt ~ qu ~ hid ~ an ~ al ~ nl ~ maxw ~ j ~ lab ~ dist ~ he ~ or =>
      SegLabel(refTo.toInt, par.toInt, loc, fnt,
                if (qu.isDefined) qu.get else null,
                if (an.isDefined) an.get.toInt else -1,
                if (al.isDefined) al.get.toInt else -1,
                if (hid.isDefined) hid.get else false,
                nl.toInt,
                maxw.toInt,
                j.toInt,
                lab,
                dist.toDouble,
                he.toInt,
                or.toInt)
  }


  /*
  object_state_view = "(object" "StateView" string string ref
                        [ "Parent_View"	ref ]
                        "location"   	point
                        [ "font"      object_font ]
                        [ "label"     ( object_seglabel | object_item_label ) ]
                        "icon_style" 	string
                        "line_color" 	number
                        [ "fill_color"    number ]
                        "quidu"      	quid_str
                        [ "width"     number ]
                        [ "height"    number ]
                        [ "autoResize"    bool ]
                       ")"
   */
  def stateViewParser: Parser[StateView] = "(object" ~> "StateView" ~> stringLiteral ~ stringLiteral ~ ref ~
                                              opt("Parent_View" ~> ref) ~
                                              ("location" ~> pointParser) ~
                                              opt("font" ~> fontParser) ~
                                              opt("label" ~> (segLabelParser|itemLabelParser)) ~
                                              ("icon_style" ~> stringLiteral) ~
                                              ("line_color" ~> wholeNumber) ~
                                              opt("fill_color" ~> wholeNumber) ~
                                              ("quidu" ~> quid) ~
                                              opt("width" ~> wholeNumber) ~
                                              opt("height" ~> wholeNumber) ~
                                              opt("autoResize" ~> bool) <~
                                            ")" ^^ {
    case name ~ descr ~ refTo ~
          parent ~
          loc ~
          font ~
          lab ~
          is ~
          lineCol ~
          fillCol ~
          qu ~
          w ~
          h ~ ar => StateView(name, descr, refTo.toInt,
                          if (parent.isDefined) parent.get.toInt else -1,
                          loc,
                          if (font.isDefined) font.get else null,
                          if (lab.isDefined) lab.get else null,
                          is,
                          lineCol.toInt,
                          if (fillCol.isDefined) fillCol.get.toInt else -1,
                          qu,
                          if (w.isDefined) w.get.toInt else -1,
                          if (h.isDefined) h.get.toInt else -1,
                          if (ar.isDefined) ar.get else false)
  }

  /*
  object_note_view = "(object" "NoteView" ref
                      [ "Parent_View"	ref ]
                      "location"   	point
                      "font"       	object_font
                      [ "label"     object_item_label ]
                      "line_color" 	number
                      "fill_color" 	number
                      "width"      	number
                      "height"     	number
                      [ "quidu"       quid_str ]
                      ")"
   */
  def noteViewParser: Parser[NoteView] = "(object" ~> "NoteView" ~> ref ~
                                            opt("Parent_View" ~> ref) ~
                                            ("location" ~> pointParser) ~
                                            ("font" ~> fontParser) ~
                                            opt("label" ~> itemLabelParser) ~
                                            ("line_color" ~> wholeNumber) ~
                                            ("fill_color" ~> wholeNumber) ~
                                            ("width" ~> wholeNumber) ~
                                            ("height" ~> wholeNumber) ~
                                            opt("quidu" ~> quid) <~
                                         ")" ^^ {
    case refTo ~ parent ~ loc ~ font ~ label ~ lineColor ~ fillColor ~ width ~ height ~ qu =>
      NoteView(refTo.toInt,
                if (parent.isDefined) parent.get.toInt else -1,
                loc, font,
                if (label.isDefined) label.get else null,
                lineColor.toInt,
                fillColor.toInt,
                width.toInt,
                height.toInt,
                if (qu.isDefined) qu.get else null)
  }


  /*
  object_attach_view = "(object" "AttachView" string ref
                          "stereotype" 	bool
                          "line_color" 	number
                          "client"     	ref
                          "supplier"   	ref
                          "vertices"   	list_point
                          "line_style" 	number
                          ")"
   */
  def attachViewParser: Parser[AttachView] = "(object" ~> "AttachView" ~> stringLiteral ~ ref ~
                                                ("stereotype" ~> bool) ~
                                                ("line_color" ~> wholeNumber) ~
                                                ("client" ~> ref) ~
                                                ("supplier" ~> ref) ~
                                                ("vertices" ~> pointListParser) ~
                                                ("line_style" ~> wholeNumber) <~
                                              ")" ^^ {
    case name ~ refTo ~ st ~ lc ~ cl ~ sup ~ vert ~ ls =>
      AttachView(name, refTo.toInt,
                  st,
                  lc.toInt,
                  cl.toInt,
                  sup.toInt,
                  vert,
                  ls.toInt)
  }


  /*
  object_decision_view = "(object" "DecisionView" string ref
                            "Parent_View" 	ref
                            "location"   	point
                            [ "font"       	object_font ]
                            [ "label"      	object_seglabel ]
                            "icon_style" 	string
                            "line_color" 	number
                            "fill_color" 	number
                            "quidu"      	quid_str
                            [ "width"      	number ]
                            [ "height"     	number ]
                          ")"
   */
  def decisionViewParser: Parser[DecisionView] = "(object" ~> "DecisionView" ~> stringLiteral ~ ref ~
                                                    ("Parent_View" ~> ref) ~
                                                    ("location" ~> pointParser) ~
                                                    opt("font" ~> fontParser) ~
                                                    opt("label" ~> segLabelParser) ~
                                                    ("icon_style" ~> stringLiteral) ~
                                                    ("line_color" ~> wholeNumber) ~
                                                    ("fill_color" ~> wholeNumber) ~
                                                    ("quidu" ~> quid) ~
                                                    opt("width" ~> wholeNumber) ~
                                                    opt("height" ~> wholeNumber) ~
                                                    opt("autoResize" ~> bool) <~
                                                  ")" ^^ {
    case name ~ refTo ~ parent ~ loc ~ font ~ lab ~ icst ~ lineClr ~ fillClr ~ qu ~ w ~ h ~ ar =>
      DecisionView(name, refTo.toInt,
                    parent.toInt,
                    loc,
                    if (font.isDefined) font.get else null,
                    if (lab.isDefined) lab.get else null,
                    icst,
                    lineClr.toInt, fillClr.toInt,
                    qu,
                    if (w.isDefined) w.get.toInt else -1,
                    if (h.isDefined) h.get.toInt else -1,
                    if (ar.isDefined) ar.get else false)
  }


  /*
  object_trans_view = "(object" "TransView" string ref
                        [ "font"      object_font ]
                        [ "label"     object_seglabel ]
                        "stereotype" 	bool
                        "line_color" 	number
                        "quidu"      	quid_str
                        "client"     	ref
                        "supplier"   	ref
                        "vertices"   	list_point
                        "line_style" 	number
                        [ "origin_attachment" point ]
                        [ "terminal_attachment" point ]
                        "x_offset"   	bool
                       ")"
   */
  def transViewParser: Parser[TransView] = "(object" ~> "TransView" ~> stringLiteral ~ ref ~
                                              opt("font" ~> fontParser) ~
                                              opt("label" ~> segLabelParser) ~
                                              ("stereotype" ~> bool) ~
                                              ("line_color" ~> wholeNumber) ~
                                              ("quidu" ~> quid) ~
                                              ("client" ~> ref) ~
                                              ("supplier" ~> ref) ~
                                              ("vertices" ~> pointListParser) ~
                                              ("line_style" ~> wholeNumber) ~
                                              opt("origin_attachment" ~> pointParser) ~
                                              opt("terminal_attachment" ~> pointParser) ~
                                              ("x_offset" ~> bool) <~
                                            ")" ^^ {
    case name ~ refTo ~ f ~ l ~ st ~ lc ~ q ~ cl ~ sup ~ vert ~ ls ~ oa ~ ta ~ xo =>
      TransView(name, refTo.toInt,
                if (f.isDefined) f.get else null,
                if (l.isDefined) l.get else null,
                st, lc.toInt, q,
                cl.toInt,
                sup.toInt,
                vert,
                ls.toInt,
                if (oa.isDefined) oa.get else null,
                if (ta.isDefined) ta.get else null,
                xo)
  }


  /*
  object_inherit_tree_view = "(object" "InheritTreeView" string ref
                                "location"   	point
                                "line_color" 	number
                                "fill_color" 	number
                                "supplier"   	ref
                                "vertices"   	list_point
                              ")"
   */
  def inheritTreeViewParser: Parser[InheritTreeView] = "(object" ~> "InheritTreeView" ~> stringLiteral ~ ref ~
                                                          ("location" ~> pointParser) ~
                                                          ("line_color" ~> wholeNumber) ~
                                                          ("fill_color" ~> wholeNumber) ~
                                                          ("supplier" ~> ref) ~
                                                          ("vertices" ~> pointListParser) <~
                                                        ")" ^^ {
    case n ~ r ~ l ~ lc ~ fc ~ s ~ v =>
      InheritTreeView(n, r.toInt, l, lc.toInt, fc.toInt, s.toInt, v)
  }


  /*
  object_activity_state_view = "(object" "ActivityStateView" string ref
                                  "Parent_View" 	ref
                                  "location"   	point
                                  "font"       	object_font
                                  "label"      	object_item_label
                                  [ "stereotype"    object_item_label ]
                                  "icon_style" 	string
                                  "line_color" 	number
                                  "fill_color" 	number
                                  "quidu"      	quid_str
                                  [ "width"      	number ]
                                  [ "height"     	number ]
                                  [ "annotation" 	number ]
                                  [ "autoResize"  bool ]
                                ")"
   */
  def activityStateViewParser: Parser[ActivityStateView] = "(object" ~> "ActivityStateView" ~> stringLiteral ~ ref ~
                                                              ("Parent_View" ~> ref) ~
                                                              ("location" ~> pointParser) ~
                                                              ("font" ~> fontParser) ~
                                                              ("label" ~> itemLabelParser) ~
                                                              opt("stereotype" ~> itemLabelParser) ~
                                                              ("icon_style" ~> stringLiteral) ~
                                                              ("line_color" ~> wholeNumber) ~
                                                              ("fill_color" ~> wholeNumber) ~
                                                              ("quidu" ~> quid) ~
                                                              opt("width" ~> wholeNumber) ~
                                                              opt("height" ~> wholeNumber) ~
                                                              opt("annotation" ~> wholeNumber) ~
                                                              opt("autoResize" ~> bool) <~
                                                            ")" ^^ {
    case name ~ refTo ~
          parent ~
          location ~
          font ~
          label ~
          st ~
          iconStyle ~
          lineColor ~
          fillColor ~
          qu ~
          w ~ h ~
          annot ~ ar => ActivityStateView(name, refTo.toInt,
                                      parent.toInt,
                                      location,
                                      font,
                                      label,
                                      if (st.isDefined) st.get else null,
                                      iconStyle,
                                      lineColor.toInt,
                                      fillColor.toInt,
                                      qu,
                                      if (w.isDefined) w.get.toInt else -1,
                                      if (h.isDefined) h.get.toInt else -1,
                                      if (annot.isDefined) annot.get.toInt else -1,
                                      if (ar.isDefined) ar.get else false)
  }

  /*
  object_use_case_view = "(object" "UseCaseView" string ref
                            "location"   	point
                            "font"       	object_font
                            "label"      	object_item_label
                            [ "icon"      string ]
                            "icon_style" 	string
                            "line_color" 	number
                            "fill_color" 	number
                            "quidu"      	quid_str
                            [ "width"     number ]
                            [ "height"    number ]
                            ")"
   */

  def useCaseViewParser: Parser[UseCaseView] = "(object" ~> "UseCaseView" ~> stringLiteral ~ ref ~
                                                  ("location" ~> pointParser) ~
                                                  ("font" ~> fontParser) ~
                                                  ("label" ~> itemLabelParser) ~
                                                  opt("icon" ~> stringLiteral) ~
                                                  ("icon_style" ~> stringLiteral) ~
                                                  ("line_color" ~> wholeNumber) ~
                                                  ("fill_color" ~> wholeNumber) ~
                                                  ("quidu" ~> quid) ~
                                                  opt("width" ~> wholeNumber) ~
                                                  opt("height" ~> wholeNumber) <~
                                                ")" ^^ {
    case n ~ r ~ l ~ f ~ lab ~ i ~ is ~ lc ~ fc ~ qu ~ w ~ h =>
      UseCaseView(n, r.toInt,
                  l,f, lab,
                  if (i.isDefined) i.get else null,
                  is,
                  lc.toInt, fc.toInt,
                  qu,
                  if (w.isDefined) w.get.toInt else -1,
                  if (h.isDefined) h.get.toInt else -1)
  }


  /*
  object_inter_mess_view = "(object" "InterMessView" string ref
                              "location"   	point
                              "font"       	object_font
                              "label"      	object_seglabel
                              "line_color" 	number
                              "client"     	ref
                              "supplier"   	ref
                              "Focus_Src"  	ref
                              "Focus_Entry" ref
                              "origin"   	  point
                              "terminus" 	  point
                              "ordinal"  	  number
                            ")"
   */
  def interMessViewParser: Parser[InterMessView] = "(object" ~> "InterMessView" ~> stringLiteral ~ ref ~
                                                      ("location" ~> pointParser) ~
                                                      ("font" ~> fontParser) ~
                                                      ("label" ~> segLabelParser) ~
                                                      ("line_color" ~> wholeNumber) ~
                                                      ("client" ~> ref) ~
                                                      ("supplier" ~> ref) ~
                                                      ("Focus_Src" ~> ref) ~
                                                      ("Focus_Entry" ~> ref) ~
                                                      ("origin" ~> pointParser) ~
                                                      ("terminus" ~> pointParser) ~
                                                      ("ordinal" ~> wholeNumber) <~
                                                    ")" ^^ {
    case n ~ r ~ l ~ f ~ lb ~ lc ~ c ~ s ~ fs ~ fe ~ orgn ~ t ~ ord =>
      InterMessView(n, r.toInt,
                    l, f, lb,
                    lc.toInt,
                    c.toInt, s.toInt, fs.toInt, fe.toInt,
                    orgn, t,
                    ord.toInt)
  }


  /*
  object_self_mess_view = "(object" "SelfMessView" string ref
                              "location"   	point
                              "font"       	object_font
                              "label"      	object_seglabel
                              "line_color" 	number
                              "client"     	ref
                              "supplier"   	ref
                              "Focus_Src"  	ref
                              "Focus_Entry"	ref
                              "origin"     	point
                              "terminus"   	point
                              "ordinal"    	number
                            ")"
   */
  def selfMessViewParser: Parser[SelfMessView] = "(object" ~> "SelfMessView" ~> stringLiteral ~ ref ~
                                                    ("location" ~> pointParser) ~
                                                    ("font" ~> fontParser) ~
                                                    ("label" ~> segLabelParser) ~
                                                    ("line_color" ~> wholeNumber) ~
                                                    ("client" ~> ref) ~
                                                    ("supplier" ~> ref) ~
                                                    ("Focus_Src" ~> ref) ~
                                                    ("Focus_Entry" ~> ref) ~
                                                    ("origin" ~> pointParser) ~
                                                    ("terminus" ~> pointParser) ~
                                                    ("ordinal" ~> wholeNumber) <~
                                                  ")" ^^ {
    case n ~ r ~ l ~ f ~ lbl ~ lc ~ c ~ s ~ fs ~ fe ~ o ~ t ~ ord =>
      SelfMessView(n, r.toInt, l, f, lbl, lc.toInt, c.toInt, s.toInt,
                   fs.toInt, fe.toInt, o, t, ord.toInt)
  }

  /*
  object_label = "(object" "Label" ref
                    [ "Parent_View" 	ref ]
                    "location"   	point
                    "font"       	object_font
                    "nlines"     	number
                    "max_width"  	number
                    "justify"    	number
                    "label"      	string
                  ")"
   */
  def labelParser: Parser[Label] = "(object" ~> "Label" ~> ref ~
                                      opt("Parent_View" ~> ref) ~
                                      ("location" ~> pointParser) ~
                                      ("font" ~> fontParser) ~
                                      ("nlines" ~> wholeNumber) ~
                                      ("max_width" ~> wholeNumber) ~
                                      ("justify" ~> wholeNumber) ~
                                      ("label" ~> stringLiteral) <~
                                    ")" ^^ {
    case r ~ p ~ l ~ f ~ n ~ w ~ j ~ lbl =>
      new Label(r.toInt,
                if (p.isDefined) p.get.toInt else -1,
                l, f, -1, n.toInt, w.toInt, j.toInt, lbl)
  }


  /*
    list_diagram_item = "(list" "diagram_item_list"
                                  {
                                    object_category_view
                                    | object_class_view
                                    | object_mod_view
                                    | object_realize_view
                                    | object_swimlane
                                    | object_state_view
                                    | object_note_view
                                    | object_attach_view
                                    | object_decision_view
                                    | object_trans_view
                                    | object_assoc_view_new
                                    | object_inherit_view
                                    | object_active_state_view
                                    | object_inter_obj_view
                                    | object_use_case_view
                                    | object_inter_mess_view
                                    | object_uses_view
                                    | object_self_mess_view
                                    | object_label
                                    | object_dependency_view
                                    | object_assoc_constraint_view
                                    | object_inherit_tree_view
                                  }
                         ")"
  */
  def diagramItemListParser: Parser[Seq[View]] = "(list" ~> "diagram_item_list" ~>
                                                    rep(categoryViewParser
                                                      |classViewParser
                                                      |modViewParser
                                                      |realizeViewParser
                                                      |swimlaneParser
                                                      |stateViewParser
                                                      |noteViewParser
                                                      |attachViewParser
                                                      |decisionViewParser
                                                      |transViewParser
                                                      |assocViewNewParser
                                                      |inheritViewParser
                                                      |activityStateViewParser
                                                      |interObjViewParser
                                                      |useCaseViewParser
                                                      |interMessViewParser
                                                      |usesViewParser
                                                      |selfMessViewParser
                                                      |labelParser
                                                      |dependencyViewParser
                                                      |assocConstraintView
                                                      |inheritTreeViewParser) <~
                                                  ")"

  /*
    object_mod_view = "(object" "ModView" string string string ref
                          "location" point
                          "font" object_font
                          "label" object_item_label
                          "icon_style" string
                          "line_color" number
                          "fill_color" number
                          "quidu" quid_str
                          "width" number
                          "height" number
                       ")"
  */
  def modViewParser: Parser[ModView] = "(object" ~> "ModView" ~> stringLiteral ~ stringLiteral ~ stringLiteral ~ ref ~
                                        "location" ~ pointParser ~
                                        "font" ~ fontParser ~
                                        "label" ~ itemLabelParser ~
                                        "icon_style" ~ stringLiteral ~
                                        "line_color" ~ wholeNumber ~
                                        "fill_color" ~ wholeNumber ~
                                        "quidu" ~ quid ~
                                        "width" ~ wholeNumber ~
                                        "height" ~ wholeNumber <~
                                        ")" ^^ {
    case name ~ modType ~ modPart ~ refTo ~
        "location" ~ loc ~
        "font" ~ font ~
        "label" ~ label ~
        "icon_style" ~ iconStyle ~
        "line_color" ~ lineClr ~
        "fill_color" ~ fillClr ~
        "quidu" ~ q ~
        "width" ~ w ~
        "height" ~ h => ModView(name, modType, modPart, refTo.toInt,
                                loc,
                                font,
                                label,
                                iconStyle,
                                lineClr.toInt,
                                fillClr.toInt,
                                q,
                                w.toInt,
                                h.toInt)
  }

  /*
    object_category_view = "(object" "CategoryView" string ref
                              "location" point
                              "font" object_font
                              "label" object_item_label
                              [ "stereotype" object_item_label ]
                              [ "icon" string ]
                              "icon_style" string
                              "line_color" number
                              "fill_color" number
                              "quidu" quid_str
                              "width" number
                              "height" number
                          ")"
  */
  def categoryViewParser: Parser[CategoryView] = "(object" ~> "CategoryView" ~> stringLiteral ~ ref ~
                                                  "location" ~ pointParser ~
                                                  "font" ~ fontParser ~
                                                  "label" ~ itemLabelParser ~
                                                  opt("stereotype" ~> itemLabelParser) ~
                                                  opt("icon" ~> stringLiteral) ~
                                                  "icon_style" ~ stringLiteral ~
                                                  "line_color" ~ wholeNumber ~
                                                  "fill_color" ~ wholeNumber ~
                                                  "quidu" ~ quid ~
                                                  "width" ~ wholeNumber ~
                                                  "height" ~ wholeNumber <~
                                                  ")" ^^ {
    case name ~ refTo ~
        "location" ~ loc ~
        "font" ~ font ~
        "label" ~ lab ~
        st ~
        ic ~
        "icon_style" ~ style ~
        "line_color" ~ lineClr ~
        "fill_color" ~ fillClr ~
        "quidu" ~ qu ~
        "width" ~ w ~
        "height" ~ h => CategoryView(name, refTo.toInt,
                                      loc,
                                      font,
                                      lab,
                                      if (st.isDefined) st.get else null,
                                      if (ic.isDefined) ic.get else null,
                                      style,
                                      lineClr.toInt,
                                      fillClr.toInt,
                                      qu,
                                      w.toInt,
                                      h.toInt)
  }


  /*
    point = "(" number "," number ")"
  */
  def pointParser: Parser[Point] = "(" ~> wholeNumber ~ "," ~ wholeNumber <~ ")" ^^ {
    case x ~ "," ~ y => new Point(x.toInt, y.toInt)
  }


  /*
    object_item_label = "(object" "ItemLabel"
                           "Parent_View" ref
                           "location" point
                           [ "fill_color" number ]
                           [ "anchor_loc" number ]
                           "nlines" number
                           "max_width" number
                           "justify" number
                           "label" text_value
                         ")"
  */
  def itemLabelParser: Parser[ItemLabel] = "(object" ~> "ItemLabel" ~>
                                            "Parent_View" ~ ref ~
                                            "location" ~ pointParser ~
                                            opt("fill_color" ~> wholeNumber) ~
                                            opt("anchor" ~> wholeNumber) ~
                                            opt("anchor_loc" ~> wholeNumber) ~
                                            "nlines" ~ wholeNumber ~
                                            "max_width" ~ wholeNumber ~
                                            "justify" ~ wholeNumber ~
                                            "label" ~ textValue <~
                                            ")" ^^ {
    case "Parent_View" ~ pw ~
          "location" ~ loc ~
          fc ~
          an ~
          anl ~
          "nlines" ~ nl ~
          "max_width" ~ mw ~
          "justify" ~ j ~
          "label" ~ lab => ItemLabel(pw.toInt,
                                      loc,
                                      if (fc.isDefined) fc.get.toInt else -1,
                                      if (an.isDefined) an.get.toInt else -1,
                                      if (anl.isDefined) anl.get.toInt else -1,
                                      nl.toInt,
                                      mw.toInt,
                                      j.toInt,
                                      lab)
  }

  /*
  object_uses_view = "(object" "UsesView" string ref
                        "font"       	object_font
                        [ "label"     object_seglabel ]
                        "stereotype" 	object_seglabel
                        "line_color" 	number
                        "quidu"      	quid_str
                        "client"     	ref
                        "supplier"   	ref
                        "vertices"   	list_point
                        "line_style" 	number
                        [ "origin_attachment" 	  point ]
                        [ "terminal_attachment" 	point ]
                      ")"
   */
  def usesViewParser: Parser[UsesView] = "(object" ~> "UsesView" ~> stringLiteral ~ ref ~
                                            opt("font" ~> fontParser) ~
                                            opt("label" ~> segLabelParser) ~
                                            ("stereotype" ~> (segLabelParser|bool)) ~
                                            ("line_color" ~> wholeNumber) ~
                                            ("quidu" ~> quid) ~
                                            ("client" ~> ref) ~
                                            ("supplier" ~> ref) ~
                                            ("vertices" ~> pointListParser) ~
                                            ("line_style" ~> wholeNumber) ~
                                            opt("origin_attachment" ~> pointParser) ~
                                            opt("terminal_attachment" ~> pointParser) <~
                                          ")" ^^ {
    case n ~ r ~ f ~ lbl ~ st ~ lc ~ qu ~ c ~ s ~ v ~ ls ~ oa ~ ta =>
      UsesView(n, r.toInt,
               if (f.isDefined) f.get else null,
               if (lbl.isDefined) lbl.get else null,
               st match {
                 case seg: Some[SegLabel] => seg.get
//                 case f: Some[Boolean] => null // TODO: I DON'T KNOW WHAT TO DO WITH IT
//                 case None => null
                 case _ => null
               },
               lc.toInt, qu, c.toInt, s.toInt, v, ls.toInt,
               if (oa.isDefined) oa.get else null,
               if (ta.isDefined) ta.get else null)
  }


  /*
    object_font = "(object" "Font"
                       "size" number
                       "face" string
                       "charSet" number
                       "bold" bool
                       "italics" bool
                       "underline" bool
                       "strike" bool
                       "color" number
                       "default_color" bool
                   ")"
  */
  def fontParser: Parser[Font] = "(object" ~> "Font" ~>
                                  "size" ~ wholeNumber ~
                                  "face" ~ stringLiteral ~
                                  "charSet" ~ wholeNumber ~
                                  "bold" ~ bool ~
                                  "italics" ~ bool ~
                                  "underline" ~ bool ~
                                  "strike" ~ bool ~
                                  "color" ~ wholeNumber ~
                                  "default_color" ~ bool <~
                                  ")" ^^ {
    case "size" ~ sz ~
          "face" ~ f ~
          "charSet" ~ cs ~
          "bold" ~ bold ~
          "italics" ~ ital ~
          "underline" ~ ul ~
          "strike" ~ str ~
          "color" ~ clr ~
          "default_color" ~ defClr => Font(sz.toInt,
                                            f,
                                            cs.toInt,
                                            bold,
                                            ital,
                                            ul,
                                            str,
                                            clr.toInt,
                                            defClr)
  }

  /*
    list_attr_set = "(list" "Attribute_Set" { object_attr } ")"
  */
  def attrListParser: Parser[Seq[BaseAttribute]] =
    "(list" ~> "Attribute_Set" ~> rep(attrParser) <~ ")"



  /*
    attr_value_string_number_pair_def = "(" string number ")"
   */
  def attrValueStringNumberPairDef = "(" ~> stringLiteral ~ wholeNumber <~ ")" ^^ {
    case name ~ value => Pair[String, Int](name, value.toInt)
  }

  /*
  attr_value_text_def = "(value Text" string ")"
*/
  def attrValueTextDef: Parser[String] = "(value" ~> "Text" ~>
                                            textValue <~
                                          ")"

  /*
    text_value = string | multiline_text
   */
  def textValue = stringLiteral | multilineText



  /*
  object_attr = "(object" "Attribute"
                    "tool" string
                    "name" string
                    "value" attr_value_def
                 ")"

  attr_value_def = number | string | list_attr_set | attr_value_string_number_pair_def
*/
  def attrValueDef = wholeNumber |
    stringLiteral |
    bool |
    attrListParser |
    attrValueStringNumberPairDef |
    attrValueTextDef

  def attrParser: Parser[BaseAttribute] = "(object" ~> "Attribute" ~>
                                            "tool" ~ stringLiteral ~
                                            "name" ~ stringLiteral ~
                                            "value" ~ attrValueDef <~
                                            ")" ^^ {
    case "tool" ~ tool ~
          "name" ~ name ~
          "value" ~ value => Attribute(tool, name, value)
  }

  /*
    quid_str = string
  */
  def quid: Parser[String] = "\"[0-9A-F]{12}\"".r

  /*
    bool = "TRUE" | "FALSE"
  */
  def bool: Parser[Boolean] = ("TRUE" | "FALSE") ^^ { case "TRUE" => true; case "FALSE" => false }

  /*
    margin_line = ^\|.*
    multiline_text = { margin_line }
  */
  def marginLineDef = """^\|.*""".r
  def multilineText: Parser[String] = rep(marginLineDef) ^^ {
    case lines => lines.mkString(System.lineSeparator).stripMargin
  }

  /**
   * StringLiteral with optional 1E (RS) char.
   * @return
   */
  override def stringLiteral: Parser[String] =
    ("\""+"""([^"\p{Cntrl}\\]|\\[\\'"bfnrt]|\\u[a-fA-F0-9]{4}|\u001e)*"""+"\"").r



  /*
    ref = @\d+
  */
  def ref: Parser[String] = "@" ~> wholeNumber


  def catParser: Parser[ClassCategory] = petalParser ~> classCategoryParser


  def parseModel(reader: Reader) = parseAll(modelParser, reader)
  def parseCatalog(reader: Reader) = parseAll(catParser, reader)
}
